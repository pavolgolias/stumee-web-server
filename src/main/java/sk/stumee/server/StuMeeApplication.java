package sk.stumee.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
//import sk.stumee.server.services.FileService;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;

@SpringBootApplication
public class StuMeeApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
//        File uploadFolder = new File(FileService.UPLOAD_FOLDER);
//        boolean removed = removeDirectory(uploadFolder);
//        File pictures = new File(uploadFolder, FileService.PICTURES_FOLDER);
//        File attachments = new File(uploadFolder, FileService.ATTACHMENTS_FOLDER);
//        boolean created = createDirectories(uploadFolder, pictures, attachments);
//
//        if (created) {
//            System.out.println("#####################################################");
//            if (removed)
//                System.out.println("############# RECREATED UPLOAD FOLDER ###############");
//            else
//                System.out.println("#############  CREATED UPLOAD FOLDER  ###############");
//            System.out.println("#####################################################");
//            SpringApplication.run(StuMeeApplication.class, args);
//        } else {
//            System.out.println("#####################################################");
//            System.out.println("#### COULD NOT CREATE UPLOAD FOLDER STRUCTURE!!! ####");
//            System.out.println("#####################################################");
//        }

        SpringApplication.run(StuMeeApplication.class, args);
    }

//    private static boolean removeDirectory(File dir) {
//        List<Boolean> results = new ArrayList<>();
//
//        if (dir.isDirectory()) {
//            File[] files = dir.listFiles();
//            if (files != null && files.length > 0) {
//                for (File f : files) {
//                    results.add(removeDirectory(f));
//                }
//            }
//        }
//
//        Boolean result = dir.delete();
//        for (Boolean r : results) {
//            result = result && r;
//        }
//        return result;
//    }
//
//    private static boolean createDirectories(File... dirs) {
//        boolean result = true;
//
//        for (File dir : dirs) {
//            result = result && dir.mkdirs();
//        }
//
//        return result;
//    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(StuMeeApplication.class);
    }
}
