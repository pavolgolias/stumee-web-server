package sk.stumee.server.dto.user;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.UserController;
import sk.stumee.server.entities.Picture;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PictureInfoResourceAssembler extends ResourceAssemblerSupport<Picture, PictureInfoResource> {
    public PictureInfoResourceAssembler() {
        super(UserController.class, PictureInfoResource.class);
    }

    @Override
    public PictureInfoResource toResource(Picture picture) {
        PictureInfoResource resource = new PictureInfoResource();
        resource.load(picture);
        resource.add(linkTo(methodOn(UserController.class).getPictureInfo(picture.getUser().getId())).withSelfRel());
        resource.add(linkTo(methodOn(UserController.class).getPictureData(picture.getUser().getId(), null)).withRel("data"));
        return resource;
    }
}
