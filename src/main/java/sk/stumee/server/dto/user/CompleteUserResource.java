package sk.stumee.server.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.entities.User;

public class CompleteUserResource extends UserResourceAdapter {
    @JsonProperty("id")
    public Long id;
    public String username;
    public String stuEmail;
    public String secondaryEmail;
    public Boolean confirmed;
    public Boolean deactivated;
    public String name;
    public String surname;
    public String description;

    public CompleteUserResource() {
    }

    public void load(User user) {
        id = user.getId();
        username = user.getUsername();
        stuEmail = user.getStuEmail();
        secondaryEmail = user.getSecondaryEmail();
        confirmed = user.getConfirmed();
        deactivated = user.getDeactivated();
        name = user.getName();
        surname = user.getSurname();
        description = user.getDescription();
    }
}
