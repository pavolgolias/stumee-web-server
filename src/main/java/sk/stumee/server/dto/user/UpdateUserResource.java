package sk.stumee.server.dto.user;

import sk.stumee.server.entities.validation.ExtendedEmail;
import sk.stumee.server.entities.validation.StuEmail;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UpdateUserResource extends UserResourceAdapter {
    @Pattern(regexp = "^[\\w\\.]{3,30}$")
    public String username;

    @StuEmail
    public String stuEmail;

    @ExtendedEmail
    public String secondaryEmail;

    public Boolean deactivated;

    @Size(min = 0)
    public String name;

    @Size(min = 0)
    public String surname;

    public String description;

    @Override
    protected Long getUserId() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getStuEmail() {
        return stuEmail;
    }

    @Override
    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    @Override
    public Boolean getDeactivated() {
        return deactivated;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
