package sk.stumee.server.dto.user;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.UserController;
import sk.stumee.server.entities.User;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class CompleteUserResourceAssembler extends ResourceAssemblerSupport<User, CompleteUserResource> {
    public CompleteUserResourceAssembler() {
        super(UserController.class, CompleteUserResource.class);
    }

    @Override
    public CompleteUserResource toResource(User user) {
        CompleteUserResource resource = createResourceWithId(user.getId(), user);
        resource.load(user);
        resource.add(linkTo(methodOn(UserController.class).getPictureInfo(user.getId())).withRel("picture"));
        resource.add(linkTo(methodOn(UserController.class).getPictureData(user.getId(), null)).withRel("pictureData"));
        return resource;
    }
}
