package sk.stumee.server.dto.user;

public class UserResourceAdapter extends UserResource {
    @Override
    protected Long getUserId() {
        return null;
    }

    @Override
    protected String getUsername() {
        return null;
    }

    @Override
    protected String getStuEmail() {
        return null;
    }

    @Override
    protected String getSecondaryEmail() {
        return null;
    }

    @Override
    protected Boolean getDeactivated() {
        return null;
    }

    @Override
    protected String getName() {
        return null;
    }

    @Override
    protected String getSurname() {
        return null;
    }

    @Override
    protected String getDescription() {
        return null;
    }
}
