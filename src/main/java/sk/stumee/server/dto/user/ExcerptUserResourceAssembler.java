package sk.stumee.server.dto.user;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.UserController;
import sk.stumee.server.entities.User;

@Component
public class ExcerptUserResourceAssembler extends ResourceAssemblerSupport<User, ExcerptUserResource> {
    public ExcerptUserResourceAssembler() {
        super(UserController.class, ExcerptUserResource.class);
    }

    @Override
    public ExcerptUserResource toResource(User user) {
        ExcerptUserResource resource = createResourceWithId(user.getId(), user);
        resource.load(user);
        return resource;
    }
}
