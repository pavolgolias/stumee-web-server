package sk.stumee.server.dto.user;

import org.springframework.hateoas.ResourceSupport;
import sk.stumee.server.entities.User;

public abstract class UserResource extends ResourceSupport {
    public User toUser() {
        User user = new User();
        user.setId(getUserId());
        user.setUsername(getUsername());
        user.setStuEmail(getStuEmail());
        user.setSecondaryEmail(getSecondaryEmail());
        user.setDeactivated(getDeactivated());
        user.setName(getName());
        user.setSurname(getSurname());
        user.setDescription(getDescription());
        return user;
    }

    protected abstract Long getUserId();
    protected abstract String getUsername();
    protected abstract String getStuEmail();
    protected abstract String getSecondaryEmail();
    protected abstract Boolean getDeactivated();
    protected abstract String getName();
    protected abstract String getSurname();
    protected abstract String getDescription();
}
