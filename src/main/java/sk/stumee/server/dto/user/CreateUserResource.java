package sk.stumee.server.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import sk.stumee.server.entities.validation.ExtendedEmail;
import sk.stumee.server.entities.validation.StuEmail;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CreateUserResource extends UserResourceAdapter {
    @JsonProperty("id")
    @NotNull
    public Long id;

    @NotNull
    @Pattern(regexp = "^[\\w\\.]{3,30}$")
    public String username;

    @NotNull
    @StuEmail
    public String stuEmail;

    @NotNull
    public String stuPassword;

    @ExtendedEmail
    public String secondaryEmail;

    @NotBlank
    public String name;

    @NotBlank
    public String surname;

    public String description;

    @Override
    protected Long getUserId() {
        return id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getStuEmail() {
        return stuEmail;
    }

    @Override
    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    @Override
    protected Boolean getDeactivated() {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
