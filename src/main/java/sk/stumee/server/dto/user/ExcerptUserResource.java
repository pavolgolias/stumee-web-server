package sk.stumee.server.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.entities.User;

public class ExcerptUserResource extends UserResourceAdapter {
    @JsonProperty("id")
    public Long id;
    public String username;
    public String name;
    public String surname;

    public ExcerptUserResource() {
    }

    public void load(User user) {
        id = user.getId();
        username = user.getUsername();
        name = user.getName();
        surname = user.getSurname();
    }
}
