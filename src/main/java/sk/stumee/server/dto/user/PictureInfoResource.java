package sk.stumee.server.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import sk.stumee.server.entities.Picture;

import java.util.Date;

public class PictureInfoResource extends ResourceSupport {
    @JsonProperty("id")
    public Long id;
    public String name;
    public String extension;
    public String contentType;
    public Long size;
    public Date created;

    public PictureInfoResource() {
    }

    public void load(Picture picture) {
        id = picture.getId();
        name = picture.getName();
        extension = picture.getExtension();
        contentType = picture.getContentType();
        size = picture.getSize();
        created = picture.getCreated();
    }
}
