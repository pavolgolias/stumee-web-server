package sk.stumee.server.dto.autocomplete;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class UserRegistrationSuggestKey {
    @NotBlank
    public String suggestKey;
    @NotNull
    public Integer maxItems;
}
