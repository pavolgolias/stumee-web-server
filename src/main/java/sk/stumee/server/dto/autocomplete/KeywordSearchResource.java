package sk.stumee.server.dto.autocomplete;

import javax.validation.constraints.NotNull;

public class KeywordSearchResource {
    @NotNull
    public String keyword;
}
