package sk.stumee.server.dto.autocomplete;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRegistrationSuggestion {
    @JsonProperty("id")
    public Long id;
    public String wholeName;
    public String department;
}
