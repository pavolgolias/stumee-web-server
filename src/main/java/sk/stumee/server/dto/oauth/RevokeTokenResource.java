package sk.stumee.server.dto.oauth;

import javax.validation.constraints.NotNull;

public class RevokeTokenResource {
    @NotNull
    public String accessToken;
}
