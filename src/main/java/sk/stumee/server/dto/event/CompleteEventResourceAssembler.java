package sk.stumee.server.dto.event;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.EventController;
import sk.stumee.server.entities.Event;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class CompleteEventResourceAssembler extends ResourceAssemblerSupport<Event, CompleteEventResource> {
    public CompleteEventResourceAssembler() {
        super(EventController.class, CompleteEventResource.class);
    }

    @Override
    public CompleteEventResource toResource(Event event) {
        CompleteEventResource resource = createResourceWithId(event.getId(), event);
        resource.load(event);
        resource.add(linkTo(methodOn(EventController.class).getEventComments(event.getId(), null, null)).withRel("comments"));
        resource.add(linkTo(methodOn(EventController.class).getEventAttachments(event.getId())).withRel("attachments"));
        resource.add(linkTo(methodOn(EventController.class).eventAuthors(event.getId(), null, null)).withRel("authors"));
        resource.add(linkTo(methodOn(EventController.class).eventInvited(event.getId(), null, null)).withRel("invited"));
        resource.add(linkTo(methodOn(EventController.class).eventGoing(event.getId(), null, null)).withRel("going"));
        resource.add(linkTo(methodOn(EventController.class).eventNotGoing(event.getId(), null, null)).withRel("notGoing"));
        resource.add(linkTo(methodOn(EventController.class).eventInvitable(event.getId(), null, null)).withRel("invitable"));
        resource.add(linkTo(methodOn(EventController.class).eventInvitableSearch(event.getId(), null, null, null)).withRel("invitable-search"));
        return resource;
    }
}
