package sk.stumee.server.dto.event.attachment;

import org.springframework.hateoas.ResourceSupport;
import sk.stumee.server.entities.Attachment;

import java.util.Date;

public abstract class AttachmentResource extends ResourceSupport {
    public Attachment toAttachment() {
        Attachment attachment = new Attachment();
        attachment.setId(getAttachmentId());
        attachment.setName(getName());
        attachment.setExtension(getExtension());
        attachment.setCaption(getCaption());
        attachment.setContentType(getContentType());
        attachment.setSize(getSize());
        attachment.setCreated(getCreated());
        return attachment;
    }

    protected abstract Long getAttachmentId();
    protected abstract String getName();
    protected abstract String getExtension();
    protected abstract String getCaption();
    protected abstract String getContentType();
    protected abstract Long getSize();
    protected abstract Date getCreated();
}
