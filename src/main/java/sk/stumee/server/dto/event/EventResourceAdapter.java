package sk.stumee.server.dto.event;

import sk.stumee.server.entities.EventType;
import sk.stumee.server.entities.EventVisibility;

import java.util.Date;

public class EventResourceAdapter extends EventResource {
    @Override
    protected Long getEventId() {
        return null;
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Override
    protected String getDescription() {
        return null;
    }

    @Override
    protected Date getCreated() {
        return null;
    }

    @Override
    protected Date getTimestamp() {
        return null;
    }

    @Override
    protected EventType getType() {
        return null;
    }

    @Override
    protected EventVisibility getVisibility() {
        return null;
    }

    @Override
    protected String getLocation() {
        return null;
    }

    @Override
    protected Boolean getCanceled() {
        return null;
    }
}
