package sk.stumee.server.dto.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;
import sk.stumee.server.entities.EventType;
import sk.stumee.server.entities.EventVisibility;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CreateEventResource extends EventResourceAdapter {
    @NotBlank
    public String title;

    @NotBlank
    public String description;

    @Past
    @NotNull
    @JsonIgnore
    public Date created = new Date();

    @Future
    @NotNull
    public Date timestamp;

    @NotNull
    public EventType type;

    @NotNull
    public EventVisibility visibility;

    @NotBlank
    public String location;

    @NotNull
    public Boolean canceled = false;

    public Set<Long> authors = new HashSet<>();

    @Override
    protected Long getEventId() {
        return null;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public EventType getType() {
        return type;
    }

    @Override
    public EventVisibility getVisibility() {
        return visibility;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public Boolean getCanceled() {
        return canceled;
    }
}
