package sk.stumee.server.dto.event.comment;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.EventController;
import sk.stumee.server.entities.Comment;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ExcerptCommentResourceAssembler extends ResourceAssemblerSupport<Comment, ExcerptCommentResource> {
    public ExcerptCommentResourceAssembler() {
        super(EventController.class, ExcerptCommentResource.class);
    }

    @Override
    public ExcerptCommentResource toResource(Comment comment) {
        ExcerptCommentResource resource = new ExcerptCommentResource();
        resource.load(comment);
        resource.add(linkTo(methodOn(EventController.class).getEventComment(comment.getEvent().getId(), comment.getId())).withSelfRel());
        return resource;
    }
}
