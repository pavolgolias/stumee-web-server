package sk.stumee.server.dto.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.EventType;
import sk.stumee.server.entities.EventVisibility;

import java.util.Date;

public class CompleteEventResource extends EventResourceAdapter {
    @JsonProperty("id")
    public Long id;
    public String title;
    public String description;
    public Date created;
    public Date timestamp;
    public EventType type;
    public EventVisibility visibility;
    public String location;
    public Boolean canceled;

    public CompleteEventResource() {
    }

    public void load(Event event) {
        id = event.getId();
        title = event.getTitle();
        description = event.getDescription();
        created = event.getCreated();
        timestamp = event.getTimestamp();
        type = event.getType();
        visibility = event.getVisibility();
        location = event.getLocation();
        canceled = event.getCanceled();
    }
}
