package sk.stumee.server.dto.event.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.EventController;
import sk.stumee.server.dto.user.ExcerptUserResourceAssembler;
import sk.stumee.server.entities.Comment;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class CompleteCommentResourceAssembler extends ResourceAssemblerSupport<Comment, CompleteCommentResource> {
    private final ExcerptUserResourceAssembler excerptUserAssembler;
    private final ExcerptCommentResourceAssembler excerptCommentAssembler;

    @Autowired
    public CompleteCommentResourceAssembler(ExcerptUserResourceAssembler excerptUserAssembler, ExcerptCommentResourceAssembler excerptCommentAssembler) {
        super(EventController.class, CompleteCommentResource.class);
        this.excerptUserAssembler = excerptUserAssembler;
        this.excerptCommentAssembler = excerptCommentAssembler;
    }

    @Override
    public CompleteCommentResource toResource(Comment comment) {
        CompleteCommentResource resource = new CompleteCommentResource();
        if (comment.getParentComment() != null) {
            resource.load(comment, excerptUserAssembler.toResource(comment.getAuthor()), excerptCommentAssembler.toResource(comment.getParentComment()));
        } else {
            resource.load(comment, excerptUserAssembler.toResource(comment.getAuthor()), null);
        }
        resource.add(linkTo(methodOn(EventController.class).getEventComment(comment.getEvent().getId(), comment.getId())).withSelfRel());
        return resource;
    }
}
