package sk.stumee.server.dto.event.comment;

import sk.stumee.server.entities.Comment;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.User;

import java.util.Date;

public class CommentResourceAdapter extends CommentResource {
    @Override
    protected Long getCommentId() {
        return null;
    }

    @Override
    protected String getContent() {
        return null;
    }

    @Override
    protected Date getTimestamp() {
        return null;
    }

    @Override
    protected Event getEvent() {
        return null;
    }

    @Override
    protected User getAuthor() {
        return null;
    }

    @Override
    protected Comment getParentComment() {
        return null;
    }
}
