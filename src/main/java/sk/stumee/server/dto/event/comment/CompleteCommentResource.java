package sk.stumee.server.dto.event.comment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.dto.user.ExcerptUserResource;
import sk.stumee.server.entities.Comment;

import java.util.Date;

public class CompleteCommentResource extends CommentResourceAdapter {
    @JsonProperty("id")
    public Long id;

    public String content;

    public Date timestamp;

    public ExcerptUserResource author;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public ExcerptCommentResource responseTo;

    public CompleteCommentResource() {
    }

    public void load(Comment comment, ExcerptUserResource author, ExcerptCommentResource responseTo) {
        id = comment.getId();
        content = comment.getContent();
        timestamp = comment.getTimestamp();
        this.author = author;
        this.responseTo = responseTo;
    }
}
