package sk.stumee.server.dto.event;

import sk.stumee.server.entities.EventType;
import sk.stumee.server.entities.EventVisibility;

import javax.validation.constraints.Future;
import javax.validation.constraints.Size;
import java.util.Date;

public class UpdateEventResource extends EventResourceAdapter {
    @Size(min = 1)
    public String title;

    @Size(min = 1)
    public String description;

    @Future
    public Date timestamp;

    public EventType type;

    public EventVisibility visibility;

    @Size(min = 1)
    public String location;

    public Boolean canceled;

    @Override
    public Long getEventId() {
        return null;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    protected Date getCreated() {
        return null;
    }

    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public EventType getType() {
        return type;
    }

    @Override
    public EventVisibility getVisibility() {
        return visibility;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public Boolean getCanceled() {
        return canceled;
    }
}
