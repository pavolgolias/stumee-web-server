package sk.stumee.server.dto.event.attachment;

import java.util.Date;

public class AttachmentResourceAdapter extends AttachmentResource {
    @Override
    protected Long getAttachmentId() {
        return null;
    }

    @Override
    protected String getName() {
        return null;
    }

    @Override
    protected String getExtension() {
        return null;
    }

    @Override
    protected String getCaption() {
        return null;
    }

    @Override
    protected String getContentType() {
        return null;
    }

    @Override
    protected Long getSize() {
        return null;
    }

    @Override
    protected Date getCreated() {
        return null;
    }
}
