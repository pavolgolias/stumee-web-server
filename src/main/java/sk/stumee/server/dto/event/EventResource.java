package sk.stumee.server.dto.event;

import org.springframework.hateoas.ResourceSupport;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.EventType;
import sk.stumee.server.entities.EventVisibility;

import java.util.Date;

public abstract class EventResource extends ResourceSupport {
    public Event toEvent() {
        Event event = new Event();
        event.setId(getEventId());
        event.setTitle(getTitle());
        event.setDescription(getDescription());
        event.setCreated(getCreated());
        event.setTimestamp(getTimestamp());
        event.setType(getType());
        event.setVisibility(getVisibility());
        event.setLocation(getLocation());
        event.setCanceled(getCanceled());
        return event;
    }

    protected abstract Long getEventId();
    protected abstract String getTitle();
    protected abstract String getDescription();
    protected abstract Date getCreated();
    protected abstract Date getTimestamp();
    protected abstract EventType getType();
    protected abstract EventVisibility getVisibility();
    protected abstract String getLocation();
    protected abstract Boolean getCanceled();
}
