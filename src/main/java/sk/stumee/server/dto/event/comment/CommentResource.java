package sk.stumee.server.dto.event.comment;

import org.springframework.hateoas.ResourceSupport;
import sk.stumee.server.entities.Comment;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.User;

import java.util.Date;

public abstract class CommentResource extends ResourceSupport {
    public Comment toComment() {
        Comment comment = new Comment();
        comment.setId(getCommentId());
        comment.setContent(getContent());
        comment.setTimestamp(getTimestamp());
        comment.setEvent(getEvent());
        comment.setAuthor(getAuthor());
        comment.setParentComment(getParentComment());
        return comment;
    }

    protected abstract Long getCommentId();
    protected abstract String getContent();
    protected abstract Date getTimestamp();
    protected abstract Event getEvent();
    protected abstract User getAuthor();
    protected abstract Comment getParentComment();
}
