package sk.stumee.server.dto.event.attachment;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.EventController;
import sk.stumee.server.entities.Attachment;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class CompleteAttachmentResourceAssembler extends ResourceAssemblerSupport<Attachment, CompleteAttachmentResource> {
    public CompleteAttachmentResourceAssembler() {
        super(EventController.class, CompleteAttachmentResource.class);
    }

    @Override
    public CompleteAttachmentResource toResource(Attachment attachment) {
        CompleteAttachmentResource resource = new CompleteAttachmentResource();
        resource.load(attachment);
        resource.add(linkTo(methodOn(EventController.class).getEventAttachmentInfo(attachment.getEvent().getId(), attachment.getId())).withSelfRel());
        resource.add(linkTo(methodOn(EventController.class).getEventAttachmentData(attachment.getEvent().getId(), attachment.getId(), null)).withRel("data"));
        return resource;
    }
}
