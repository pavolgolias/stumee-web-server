package sk.stumee.server.dto.event.attachment;

import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.entities.Attachment;

import java.util.Date;

public class CompleteAttachmentResource extends AttachmentResourceAdapter {
    @JsonProperty("id")
    public Long id;
    public String name;
    public String extension;
    public String caption;
    public String type;
    public Long size;
    public Date created;

    public CompleteAttachmentResource() {
    }

    public void load(Attachment attachment) {
        id = attachment.getId();
        name = attachment.getName();
        extension = attachment.getExtension();
        caption = attachment.getCaption();
        type = attachment.getContentType();
        size = attachment.getSize();
        created = attachment.getCreated();
    }
}
