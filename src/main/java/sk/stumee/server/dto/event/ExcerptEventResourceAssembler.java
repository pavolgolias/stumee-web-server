package sk.stumee.server.dto.event;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import sk.stumee.server.controllers.EventController;
import sk.stumee.server.entities.Event;

@Component
public class ExcerptEventResourceAssembler extends ResourceAssemblerSupport<Event, ExcerptEventResource> {
    public ExcerptEventResourceAssembler() {
        super(EventController.class, ExcerptEventResource.class);
    }

    @Override
    public ExcerptEventResource toResource(Event event) {
        ExcerptEventResource resource = createResourceWithId(event.getId(), event);
        resource.load(event);
        return resource;
    }
}
