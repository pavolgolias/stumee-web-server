package sk.stumee.server.dto.event.attachment;

import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.entities.Attachment;

public class ExcerptAttachmentResource extends AttachmentResourceAdapter {
    @JsonProperty("id")
    public Long id;

    public String caption;

    public ExcerptAttachmentResource() {
    }

    public void load(Attachment attachment) {
        id = attachment.getId();
        caption = attachment.getCaption();
    }
}
