package sk.stumee.server.dto.event.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import sk.stumee.server.entities.Comment;

public class ExcerptCommentResource extends CommentResourceAdapter {
    @JsonProperty("id")
    public Long id;

    public ExcerptCommentResource() {
    }

    public void load(Comment comment) {
        id = comment.getId();
    }
}
