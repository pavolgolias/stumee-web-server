package sk.stumee.server.dto.event.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

public class CreateCommentResource extends CommentResourceAdapter {
    @NotBlank
    public String content;

    @JsonIgnore
    public Date timestamp = new Date();

    public Long responseToId;

    @Override
    protected Long getCommentId() {
        return null;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public Date getTimestamp() {
        return timestamp;
    }
}
