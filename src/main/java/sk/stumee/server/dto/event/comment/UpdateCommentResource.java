package sk.stumee.server.dto.event.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

public class UpdateCommentResource extends CommentResourceAdapter {
    @NotBlank
    public String content;

    @JsonIgnore
    public Date timestamp = new Date();

    @Override
    protected String getContent() {
        return content;
    }

    @Override
    protected Date getTimestamp() {
        return timestamp;
    }
}
