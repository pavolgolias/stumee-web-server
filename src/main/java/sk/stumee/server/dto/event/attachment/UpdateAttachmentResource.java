package sk.stumee.server.dto.event.attachment;

import org.hibernate.validator.constraints.NotBlank;

public class UpdateAttachmentResource extends AttachmentResourceAdapter {
    @NotBlank
    public String caption;

    @Override
    protected String getCaption() {
        return caption;
    }
}
