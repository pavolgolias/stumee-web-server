package sk.stumee.server.services;

import java.io.Serializable;
import java.util.Date;

public interface StuAuthService {
    SuccessfulStuAuthResult getAisAuthToken(String username, String password);

    class SuccessfulStuAuthResult implements Serializable {
        public String aisAuthToken;
        public Date aisAuthTokenExpiration;

        public SuccessfulStuAuthResult(String aisAuthToken, Date aisAuthTokenExpiration) {
            this.aisAuthToken = aisAuthToken;
            this.aisAuthTokenExpiration = aisAuthTokenExpiration;
        }
    }
}
