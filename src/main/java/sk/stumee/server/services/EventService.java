package sk.stumee.server.services;

import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import sk.stumee.server.entities.Attachment;
import sk.stumee.server.entities.Comment;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.User;

import java.io.IOException;
import java.util.Set;

public interface EventService {
    Event getEvent(Long eventId);

    Event createEvent(Event event, Set<Long> authorsIds);

    Event updateEvent(Long originalEventId, Event event);

    Event cancelEvent(Long eventId);


    Page<Event> searchEvents(String keyword, Pageable pageable);

    Page<Event> getEvents(Pageable pageable);

    Page<Event> getUserAcceptedAccessibleFutureEvents(Pageable pageable);

    Page<Event> getUserUnconfirmedAccessibleEvents(Pageable pageable);

    Page<Event> getUserOwnEvents(Pageable pageable);

    Page<Event> getUserDeclinedAccessibleFutureEvents(Pageable pageable);

    Page<Event> getUserAccessiblePastEvents(Pageable pageable);


    Page<User> getAuthors(Long eventId, Pageable pageable);

    Page<User> getInvitedUsers(Long eventId, Pageable pageable);

    Page<User> getInvitableUsers(Long eventId, Pageable pageable);

    Page<User> searchInvitableUsers(Long eventId, String keyword, Pageable pageable);

    Page<User> getGoingUsers(Long eventId, Pageable pageable);

    Page<User> getNotGoingUsers(Long eventId, Pageable pageable);

    void addEventAuthors(Long eventId, Set<Long> authorsIds);

    void deleteEventAuthors(Long eventId, Set<Long> authorsIds);

    void inviteUsersToEvent(Long eventId, Set<Long> toInviteIds);

    void markUserAsGoingToEvent(Long eventId, Long toMarkId);

    void markUserAsNotGoingToEvent(Long eventId, Long toMarkId);


    Page<Comment> getEventComments(Long eventId, Pageable pageable);

    Comment createEventComment(Long eventId, Long authorId, Long parentCommentId, Comment comment);

    Comment getEventComment(Long commentId);

    Comment updateEventComment(Long commentId, Comment comment);

    void deleteEventComment(Long commentId);

    Iterable<Attachment> getEventAttachments(Long eventInd);
    Attachment createEventAttachment(Long eventId, MultipartFile attachment, String caption) throws IOException;
    Attachment getEventAttachmentInfo(Long attachmentId);
    FileSystemResource getEventAttachmentData(Attachment attachmentId);
    Attachment updateEventAttachmentInfo(Long attachmentId, Attachment attachment);
    void deleteEventAttachment(Long attachmentId);
}
