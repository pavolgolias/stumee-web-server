package sk.stumee.server.services.impl;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.services.FileService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
@Transactional
public class FileServiceImpl implements FileService {
    @Override
    public Long storeFile(InputStream in, String filename, Long size) throws IOException {
//        System.out.println(System.getProperty("user.dir"));
        File uploadFolder = new File(FileService.UPLOAD_FOLDER);
        if (size > uploadFolder.getFreeSpace())
            throw new RuntimeException("Not enough space to store file on the server");

        File file = new File(uploadFolder, filename);
        if (!file.createNewFile()) {
            throw new RuntimeException("Can not create file");
        }
        FileOutputStream out = new FileOutputStream(file);
        byte[] bytes = new byte[1024];

        long totalRead = 0;
        int r;
        while ((r = in.read(bytes)) != -1) {
            out.write(bytes);
            totalRead += r;
        }
        out.close();

        if (size == -1)
            return totalRead;
        return size;
    }

    @Override
    public FileSystemResource getFileContent(String filename) {
        return new FileSystemResource(new File(UPLOAD_FOLDER, filename));
    }

    @Override
    public void deleteFile(String filename) {
        File file = new File(UPLOAD_FOLDER, filename);
        if (!file.delete())
            throw new RuntimeException("Can not delete file " + file);
    }
}
