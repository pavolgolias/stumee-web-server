package sk.stumee.server.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import sk.stumee.server.dao.UserRepository;
import sk.stumee.server.entities.Picture;
import sk.stumee.server.entities.User;
import sk.stumee.server.security.StuMeeUserDetails;
import sk.stumee.server.services.FileService;
import sk.stumee.server.services.UserService;

import java.io.IOException;
import java.io.InputStream;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final FileService fileService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, FileService fileService) {
        this.userRepository = userRepository;
        this.fileService = fileService;
    }

    @Override
    public void throwExceptionIfRegistered(User user) {
        if (userRepository.exists(user.getId()))
            throw new RequestException.Conflict("User with id " + user.getId() + " already exists");
        if (userRepository.findByUsername(user.getUsername()) != null)
            throw new RequestException.Conflict("User with username " + user.getUsername() + " already exists");
        if (userRepository.findByStuEmail(user.getStuEmail()) != null)
            throw new RequestException.Conflict("User with email " + user.getStuEmail() + " is already registered");
    }

    @Override
    public User getUser(Long userId) {
        User result = userRepository.findOne(userId);

        if (result == null)
            throw new RequestException.NotFound("User with id " + userId + " not found");

        return result;
    }

    @Override
    public User saveUser(User user, Picture picture) {
        user = userRepository.save(user);
        picture.setUser(user);
        user.setPicture(picture);

        return userRepository.save(user);
    }

    @Override
    public User updateUser(Long originalUserId, User user) {
        if (!originalUserId.equals(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()))
            throw new RequestException.Unauthorized("Only self update is allowed");

        User original = getUser(originalUserId);

        user.setId(originalUserId);

        if (user.getUsername() != null) {
            User u = userRepository.findByUsername(user.getUsername());
            if (u != null && !u.getId().equals(user.getId()))
                throw new RequestException.Conflict("User with username " + user.getUsername() + " already exists");
        }
        Utils.copyNonNullProperties(user, original);

        return userRepository.save(original);
    }

    @Override
    public User deactivateUser(Long userId) {
        if (!userId.equals(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()))
            throw new RequestException.Unauthorized("Only self deactivation is allowed");

        User user = this.getUser(userId);
        user.setDeactivated(true);
        return userRepository.save(user);
    }

    @Override
    public Page<User> searchUsers(String keyword, Pageable pageable) {
        return userRepository.findByKeyword(keyword, pageable);
    }

    @Override
    public Page<User> getUsers(Pageable pageable) {
        return userRepository.findByDeactivatedFalse(pageable);
    }

    @Override
    public Picture getUserPictureInfo(Long userId) {
        User user = getUser(userId);
        return getUserPictureInfoInternal(user);
    }

    @Override
    public FileSystemResource getUserPictureData(Picture picture) {
        return fileService.getFileContent(getUserPicturePath(picture));
    }

    @Override
    public Picture updateUserPicture(Long userId, MultipartFile pic) throws IOException {
        if (!userId.equals(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()))
            throw new RequestException.Unauthorized("Only self picture update is allowed");

        User user = getUser(userId);
        Picture picture = user.getPicture();

        if (picture != null) {
            fileService.deleteFile(getUserPicturePath(picture));
            user.setPicture(null);
            user = userRepository.save(user);
        }

        picture = createUserPictureInfo(pic);
        picture.setUser(user);
        user.setPicture(picture);
        user = userRepository.save(user);
        picture = user.getPicture();

        InputStream in = pic.getInputStream();
        fileService.storeFile(in, getUserPicturePath(picture), pic.getSize());
        in.close();

        return picture;
    }

    @Override
    public void deleteUserPicture(Long userId) {
        if (!userId.equals(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()))
            throw new RequestException.Unauthorized("Only self picture delete is allowed");

        User user = getUser(userId);
        Picture picture = getUserPictureInfoInternal(user);

        fileService.deleteFile(getUserPicturePath(picture));
        user.setPicture(null);
        userRepository.save(user);
    }

    private Picture getUserPictureInfoInternal(User user) {
        Picture picture = user.getPicture();

        if (picture == null)
            throw new RequestException.NoContent("No user picture available");

        return user.getPicture();
    }

    private String getUserPicturePath(Picture picture) {
        return FileService.PICTURES_FOLDER + "/" + picture.getUser().getId() + picture.getExtension();
    }

    private Picture createUserPictureInfo(MultipartFile pic) {
        Picture picture = new Picture();

        String filename = pic.getOriginalFilename();
        String name = filename;
        String ext = "";

        int lastDot = filename.lastIndexOf('.');
        if (lastDot != -1 && lastDot != 0) {
            name = filename.substring(0, lastDot);
            ext = filename.substring(lastDot);
        }

        picture.setName(name);
        picture.setExtension(ext);
        picture.setContentType(pic.getContentType());
        picture.setSize(pic.getSize());

        return picture;
    }
}
