package sk.stumee.server.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.services.StuAuthService;

import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
@PropertySource("classpath:stu-auth.properties")
public class StuAuthServiceImpl implements StuAuthService {
    @Autowired
    private Environment conf;

    @Override
    public SuccessfulStuAuthResult getAisAuthToken(String username, String password) {
        try {
            URL url = new URL(conf.getProperty("stu-auth.request.target"));
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod(conf.getProperty("stu-auth.request.method"));
            connection.setRequestProperty("User-Agent", conf.getProperty("stu-auth.request.user-agent"));
            connection.setInstanceFollowRedirects(false);

            RequestParams params = new RequestParams();
            params.addParam(conf.getProperty("stu-auth.request.params.lang.name"), conf.getProperty("stu-auth.request.params.lang.value"));
            params.addParam(conf.getProperty("stu-auth.request.params.destination.name"), conf.getProperty("stu-auth.request.params.destination.value"));
            params.addParam(conf.getProperty("stu-auth.request.params.username.name"), username);
            params.addParam(conf.getProperty("stu-auth.request.params.password.name"), password);
            params.addParam(conf.getProperty("stu-auth.request.params.expiry-time.name"), conf.getProperty("stu-auth.request.params.expiry-time.value"));
            params.addParam(conf.getProperty("stu-auth.request.params.submit.name"), conf.getProperty("stu-auth.request.params.submit.value"));

            connection.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.writeBytes(params.getUrlEncodedString(conf.getProperty("stu-auth.request.params.encoding")));
            out.flush();
            out.close();

            if (connection.getResponseCode() == Integer.parseInt(conf.getProperty("stu-auth.response.success-http-code"))) {
                String aisAuthToken = connection.getHeaderField("Set-Cookie");

                if (aisAuthToken == null)
                    throw new AuthenticationServiceException("Failed to authenticate with AIS. Auth cookie not found in headers");

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.SECOND, Integer.parseInt(conf.getProperty("stu-auth.request.params.expiry-time.value")));
                Date aisAuthTokenExpiration = calendar.getTime();

                return new SuccessfulStuAuthResult(aisAuthToken, aisAuthTokenExpiration);
            } else if (connection.getResponseCode() == Integer.parseInt(conf.getProperty("stu-auth.response.failure-http-code"))) {
                throw new AuthenticationServiceException("Wrong username or password");
            }
            throw new AuthenticationServiceException("Failed to authenticate with AIS. Response code: " + connection.getResponseCode());
        } catch (MalformedURLException e) {
            throw new AuthenticationServiceException("MalformedURLException: " + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw new AuthenticationServiceException("UnsupportedEncodingException: " + e.getMessage());
        } catch (ProtocolException e) {
            throw new AuthenticationServiceException("ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            throw new AuthenticationServiceException("IOException: " + e.getMessage());
        }
    }

    private class RequestParams {
        private Map<String, String> params;

        private RequestParams() {
            this.params = new HashMap<>();
        }

        private void addParam(String name, String value) {
            params.put(name, value);
        }

        private String getUrlEncodedString(String encoding) throws UnsupportedEncodingException {
            StringBuilder encodedParams = new StringBuilder();

            for (Map.Entry<String, String> entry : params.entrySet()) {
                encodedParams.append("&");
                encodedParams.append(entry.getKey());
                encodedParams.append("=");
                encodedParams.append(URLEncoder.encode(entry.getValue(), encoding));
            }

            return encodedParams.substring(1);
        }
    }
}
