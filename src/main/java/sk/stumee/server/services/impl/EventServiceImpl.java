package sk.stumee.server.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import sk.stumee.server.dao.AttachmentRepository;
import sk.stumee.server.dao.CommentRepository;
import sk.stumee.server.dao.EventRepository;
import sk.stumee.server.dao.UserRepository;
import sk.stumee.server.entities.*;
import sk.stumee.server.security.StuMeeUserDetails;
import sk.stumee.server.services.EventService;
import sk.stumee.server.services.FileService;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final AttachmentRepository attachmentRepository;
    private final FileService fileService;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository, UserRepository userRepository,
                            CommentRepository commentRepository, FileService fileService,
                            AttachmentRepository attachmentRepository) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
        this.fileService = fileService;
        this.attachmentRepository = attachmentRepository;
    }

    private User getUserWithId(Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null)
            throw new RequestException.NotFound("User with id " + userId + " not found");
        return user;
    }

    private Set<User> getUsersWithIds(Set<Long> usersIds) {
        Set<User> users = new HashSet<>();
        Utils.iterableToCollection(userRepository.findAll(usersIds), users);
        if (users.size() != usersIds.size())
            throw new RequestException.NotFound(usersIds.size() - users.size() + " users not found");
        return users;
    }

    @Override
    public Event getEvent(Long eventId) {
        Event event = eventRepository.findOne(eventId);

        if (event == null)
            throw new RequestException.NotFound("Event with eventId " + eventId + " not found");

        if (event.getVisibility() == EventVisibility.PRIVATE) {
            User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
            boolean isEligible = event.getInvited().contains(loggedUser) || event.getGoing().contains(loggedUser)
                    || event.getNotGoing().contains(loggedUser) || event.getAuthors().contains(loggedUser);
            if (!isEligible)
                throw new RequestException.Unauthorized("Event is inaccessible");
        }

        return event;
    }

    @Override
    public Event createEvent(Event event, Set<Long> authorsIds) {
        Set<User> authors = getUsersWithIds(authorsIds);
        event.setAuthors(authors);
        return eventRepository.save(event);
    }

    @Override
    public Event updateEvent(Long originalEventId, Event event) {
        Event original = getEvent(originalEventId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!original.getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can update the event");

        Utils.copyNonNullProperties(event, original);
        return eventRepository.save(original);
    }

    @Override
    public Event cancelEvent(Long eventId) {
        Event event = getEvent(eventId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!event.getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can cancel the event");

        event.setCanceled(true);
        return eventRepository.save(event);
    }

    @Override
    public Page<Event> getEvents(Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findUserAccessibleEvents(loggedUser, pageable);
    }

    @Override
    public Page<Event> getUserAcceptedAccessibleFutureEvents(Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findUserAcceptedAccessibleFutureEvents(loggedUser, pageable);
    }

    @Override
    public Page<Event> getUserUnconfirmedAccessibleEvents(Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findUserUnconfirmedAccessibleEvents(loggedUser, pageable);
    }

    @Override
    public Page<Event> getUserOwnEvents(Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findUserOwnEvents(loggedUser, pageable);
    }

    @Override
    public Page<Event> getUserDeclinedAccessibleFutureEvents(Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findUserDeclinedAccessibleFutureEvents(loggedUser, pageable);
    }

    @Override
    public Page<Event> getUserAccessiblePastEvents(Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findUserAccessiblePastEvents(loggedUser, pageable);
    }

    @Override
    public Page<Event> searchEvents(String keyword, Pageable pageable) {
        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        return eventRepository.findByKeyword(loggedUser, keyword, pageable);
    }

    @Override
    public Page<User> getAuthors(Long eventId, Pageable pageable) {
        Event event = getEvent(eventId);
        return userRepository.findEventAuthors(event, pageable);
    }

    @Override
    public Page<User> getInvitedUsers(Long eventId, Pageable pageable) {
        Event event = getEvent(eventId);
        return userRepository.findEventInvitedUsers(event, pageable);
    }

    @Override
    public Page<User> getInvitableUsers(Long eventId, Pageable pageable) {
        Event event = getEvent(eventId);
        return userRepository.findEventInvitableUsers(event, pageable);
    }

    @Override
    public Page<User> searchInvitableUsers(Long eventId, String keyword, Pageable pageable) {
        Event event = getEvent(eventId);
        return userRepository.findEventInvitableUsersByKeyword(event, keyword, pageable);
    }

    @Override
    public Page<User> getGoingUsers(Long eventId, Pageable pageable) {
        Event event = getEvent(eventId);
        return userRepository.findEventGoingUsers(event, pageable);
    }

    @Override
    public Page<User> getNotGoingUsers(Long eventId, Pageable pageable) {
        Event event = getEvent(eventId);
        return userRepository.findEventNotGoingUsers(event, pageable);
    }

    @Override
    public void addEventAuthors(Long eventId, Set<Long> toAddIds) {
        Event event = getEvent(eventId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!event.getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can add more event authors");

        Set<User> toAdd = getUsersWithIds(toAddIds);

        event.getAuthors().addAll(toAdd);
        eventRepository.save(event);
    }

    @Override
    public void deleteEventAuthors(Long eventId, Set<Long> toDeleteIds) {
        Event event = getEvent(eventId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!event.getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can remove other event authors");

        Set<User> toDelete = getUsersWithIds(toDeleteIds);

        Set<User> authors = event.getAuthors();
        if (toDelete.containsAll(authors))
            throw new RequestException.UnprocessableEntity("There must exist at least one event author");
        authors.removeAll(toDelete);
        eventRepository.save(event);
    }

    @Override
    public void inviteUsersToEvent(Long eventId, Set<Long> toInviteIds) {
        Event event = getEvent(eventId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!event.getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can invite users to the event");

        Set<User> toInvite = getUsersWithIds(toInviteIds);

        toInvite.removeAll(event.getAuthors());
        toInvite.removeAll(event.getGoing());
        toInvite.removeAll(event.getNotGoing());

        Set<User> invited = event.getInvited();
        invited.addAll(toInvite);
        eventRepository.save(event);
    }

    @Override
    public void markUserAsGoingToEvent(Long eventId, Long toMarkId) {
        Event event = getEvent(eventId);
        User toMark = getUserWithId(toMarkId);

        Set<User> going = event.getGoing();
        if (going.contains(toMark)) {
            return;
        }

        Set<User> notGoing = event.getNotGoing();
        if (notGoing.contains(toMark)) {
            going.add(toMark);
            notGoing.remove(toMark);
            eventRepository.save(event);
            return;
        }

        Set<User> invited = event.getInvited();
        if (event.getVisibility() == EventVisibility.PRIVATE && !invited.contains(toMark) && !event.getAuthors().contains(toMark))
            throw new RequestException.UnprocessableEntity("User needs to be invited to private event or be author of event");

        invited.remove(toMark);
        going.add(toMark);
        eventRepository.save(event);
    }

    @Override
    public void markUserAsNotGoingToEvent(Long eventId, Long toMarkId) {
        Event event = getEvent(eventId);
        User toMark = getUserWithId(toMarkId);

        Set<User> notGoing = event.getNotGoing();
        if (notGoing.contains(toMark)) {
            return;
        }

        Set<User> going = event.getGoing();
        if (going.contains(toMark)) {
            going.remove(toMark);
            notGoing.add(toMark);
            eventRepository.save(event);
            return;
        }

        Set<User> invited = event.getInvited();
        if (event.getVisibility() == EventVisibility.PRIVATE && !invited.contains(toMark) && !event.getAuthors().contains(toMark))
            throw new RequestException.UnprocessableEntity("User needs to be invited to private event or be author of event");

        invited.remove(toMark);
        notGoing.add(toMark);
        eventRepository.save(event);
    }

    @Override
    public Page<Comment> getEventComments(Long eventId, Pageable pageable) {
        Event event = getEvent(eventId);
        return commentRepository.findAllByEvent(event, pageable);
    }

    @Override
    public Comment createEventComment(Long eventId, Long authorId, Long parentCommentId, Comment comment) {
        Event event = getEvent(eventId);
        User author = getUserWithId(authorId);

        comment.setEvent(event);
        comment.setAuthor(author);

        if (parentCommentId != null) {
            comment.setParentComment(getEventComment(parentCommentId));
        }

        comment = commentRepository.save(comment);

        return comment;
    }

    @Override
    public Comment getEventComment(Long commentId) {
        Comment comment = commentRepository.findOne(commentId);

        if (comment == null)
            throw new RequestException.NotFound("Comment with id " + commentId + " not found");

        return comment;
    }

    @Override
    public Comment updateEventComment(Long commentId, Comment c) {
        Comment comment = getEventComment(commentId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!comment.getAuthor().getId().equals(loggedUser.getId()))
            throw new RequestException.Unauthorized("Only an comment author can update event comment");

        Utils.copyNonNullProperties(c, comment);
        return commentRepository.save(comment);
    }

    @Override
    public void deleteEventComment(Long commentId) {
        Comment comment = getEventComment(commentId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!comment.getAuthor().getId().equals(loggedUser.getId()))
            throw new RequestException.Unauthorized("Only an comment author can delete event comment");

        commentRepository.delete(comment);
    }

    @Override
    public Iterable<Attachment> getEventAttachments(Long eventInd) {
        Event event = getEvent(eventInd);
        return attachmentRepository.findAllByEvent(event);
    }

    @Override
    public Attachment createEventAttachment(Long eventId, MultipartFile attachment, String caption) throws IOException {
        Event event = getEvent(eventId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!event.getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can add event attachments");

        Attachment a = createEventAttachmentInfo(attachment, caption);
        a.setEvent(event);
        a = attachmentRepository.save(a);

        InputStream in = attachment.getInputStream();
        fileService.storeFile(in, getEventAttachmentPath(a), attachment.getSize());
        in.close();

        return a;
    }

    @Override
    public Attachment getEventAttachmentInfo(Long attachmentId) {
        Attachment attachment = attachmentRepository.findOne(attachmentId);

        if (attachment == null)
            throw new RequestException.NotFound("Attachment with id " + attachmentId + " not found");

        return attachment;
    }

    @Override
    public FileSystemResource getEventAttachmentData(Attachment attachment) {
        return fileService.getFileContent(getEventAttachmentPath(attachment));
    }

    @Override
    public Attachment updateEventAttachmentInfo(Long attachmentId, Attachment attachment) {
        Attachment a = getEventAttachmentInfo(attachmentId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!a.getEvent().getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can add update event attachments");

        Utils.copyNonNullProperties(attachment, a);
        return attachmentRepository.save(a);
    }

    @Override
    public void deleteEventAttachment(Long attachmentId) {
        Attachment attachment = getEventAttachmentInfo(attachmentId);

        User loggedUser = getUserWithId(((StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        if (!attachment.getEvent().getAuthors().contains(loggedUser))
            throw new RequestException.Unauthorized("Only an event author can add delete event attachments");

        fileService.deleteFile(getEventAttachmentPath(attachment));
        attachmentRepository.delete(attachment);
    }

    private String getEventAttachmentPath(Attachment attachment) {
        return FileService.ATTACHMENTS_FOLDER + "/" + attachment.getId() + attachment.getExtension();
    }

    private Attachment createEventAttachmentInfo(MultipartFile a, String caption) {
        Attachment attachment = new Attachment();

        String filename = a.getOriginalFilename();
        String name = filename;
        String ext = "";

        int lastDot = filename.lastIndexOf('.');
        if (lastDot != -1 && lastDot != 0) {
            name = filename.substring(0, lastDot);
            ext = filename.substring(lastDot);
        }

        attachment.setName(name);
        attachment.setExtension(ext);
        attachment.setContentType(a.getContentType());
        attachment.setSize(a.getSize());
        attachment.setCaption(caption);

        return attachment;
    }
}
