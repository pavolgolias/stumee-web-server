package sk.stumee.server.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.dto.autocomplete.UserRegistrationSuggestKey;
import sk.stumee.server.dto.autocomplete.UserRegistrationSuggestion;
import sk.stumee.server.entities.Picture;
import sk.stumee.server.services.FileService;
import sk.stumee.server.services.StuAisParserService;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@PropertySource("classpath:stu-parser.properties")
public class StuAisParserServiceImpl implements StuAisParserService {
    @Autowired
    private ObjectMapper jsonMapper;
    @Autowired
    private Environment conf;
    @Autowired
    private FileService fileService;

    @Override
    public List<UserRegistrationSuggestion> suggestUsers(UserRegistrationSuggestKey suggestKey) {
        String key = suggestKey.suggestKey;

        int end = key.indexOf('@');
        if (end != -1) key = key.substring(0, end);
        key = key.replaceAll("[^a-zA-Z0-9]", " ");

        StringBuilder params = new StringBuilder(conf.getProperty("stu-autocompleter.request.params.default"));

        params.append("&");
        params.append(conf.getProperty("stu-autocompleter.request.params.max-items.name"));
        params.append("=");
        params.append(suggestKey.maxItems);

        params.append("&");
        params.append(conf.getProperty("stu-autocompleter.request.params.suggest-key.name"));
        params.append("=");
        params.append(key);

        params.append("&");
        params.append(conf.getProperty("stu-autocompleter.request.params.suggest-key-second.name"));
        params.append("=");
        params.append(key);

        AisUiSuggestReturnObject store;
        try {
            HttpResponse<String> response = Unirest.post(conf.getProperty("stu-autocompleter.request.target"))
                    .header("user-agent", conf.getProperty("stu-autocompleter.request.user-agent"))
                    .header("cache-control", "no-cache")
                    .header("content-type", conf.getProperty("stu-autocompleter.request.content-type"))
                    .body(params.toString())
                    .asString();
            if (Integer.parseInt(conf.getProperty("stu-autocompleter.response.success-http-code")) == response.getStatus()) {
                store = jsonMapper.readValue(response.getBody(), AisUiSuggestReturnObject.class);
            } else
                throw new RuntimeException("AIS users suggest request failed with status: " + response.getStatus());
        } catch (UnirestException | IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        List<UserRegistrationSuggestion> result = new ArrayList<>();
        for (List<String> row : store.data) {
            UserRegistrationSuggestion resource = new UserRegistrationSuggestion();
            resource.id = Long.parseLong(row.get(1));
            resource.wholeName = row.get(0);
            resource.department = row.get(3);
            result.add(resource);
        }

        return result;
    }

    @Override
    public Picture retrieveAndStoreUserPicture(Long id, String aisAuthToken) {
        Picture picture = new Picture();
        picture.setName("user-" + id.toString());

        try {
            HttpResponse<InputStream> response = Unirest.get(conf.getProperty("stu-photo.request.target"))
                    .header("user-agent", conf.getProperty("stu-photo.request.user-agent"))
                    .header("cache-control", "no-cache")
                    .header("cookie", aisAuthToken)
                    .queryString(conf.getProperty("stu-photo.request.param.id.name"), id)
                    .asBinary();

            InputStream in = response.getBody();
            String contentType = response.getHeaders().getFirst("Content-Type");
            picture.setExtension("." + contentType.split("/")[1]);
            picture.setContentType(contentType);
            picture.setSize(fileService.storeFile(in, FileService.PICTURES_FOLDER + "/" + id + picture.getExtension(), -1L));
            in.close();
        } catch (UnirestException | IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return picture;
    }

    private static class AisUiSuggestReturnObject {
        public List<List<String>> data;
        public int more;
    }
}
