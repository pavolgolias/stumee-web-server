package sk.stumee.server.services.impl;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

class RequestException {
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    static class NotFound extends RuntimeException {
        NotFound(String message) {
            super(message);
        }
    }

    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    static class UnprocessableEntity extends RuntimeException {
        UnprocessableEntity(String message) {
            super(message);
        }
    }

    @ResponseStatus(value = HttpStatus.CONFLICT)
    static class Conflict extends RuntimeException {
        Conflict(String message) {
            super(message);
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    static class BadRequest extends RuntimeException {
        BadRequest(String message) {
            super(message);
        }
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    static class NoContent extends RuntimeException {
        NoContent(String message) {
            super(message);
        }
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    static class Unauthorized extends RuntimeException {
        Unauthorized(String message) {
            super(message);
        }
    }
}
