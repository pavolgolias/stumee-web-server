package sk.stumee.server.services;

import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface FileService {
    String UPLOAD_FOLDER = new File(System.getenv("OPENSHIFT_DATA_DIR"), "upload").getAbsolutePath();
    String ATTACHMENTS_FOLDER = "event_attachments";
    String PICTURES_FOLDER = "user_pictures";

    Long storeFile(InputStream in, String filename, Long size) throws IOException;
    FileSystemResource getFileContent(String filename);
    void deleteFile(String filename);
}
