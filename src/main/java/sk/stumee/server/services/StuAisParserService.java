package sk.stumee.server.services;

import sk.stumee.server.dto.autocomplete.UserRegistrationSuggestKey;
import sk.stumee.server.dto.autocomplete.UserRegistrationSuggestion;
import sk.stumee.server.entities.Picture;

import java.util.List;

public interface StuAisParserService {
    List<UserRegistrationSuggestion> suggestUsers(UserRegistrationSuggestKey suggestKey);
    Picture retrieveAndStoreUserPicture(Long id, String aisAuthToken);
}
