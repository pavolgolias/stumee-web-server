package sk.stumee.server.services;

import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import sk.stumee.server.entities.Picture;
import sk.stumee.server.entities.User;

import java.io.IOException;

public interface UserService {
    void throwExceptionIfRegistered(User user);
    User getUser(Long userId);
    User saveUser(User user, Picture picture);
    User updateUser(Long originalUserId, User user);
    User deactivateUser(Long userId);

    Page<User> searchUsers(String keyword, Pageable pageable);
    Page<User> getUsers(Pageable pageable);

    Picture getUserPictureInfo(Long userId);
    FileSystemResource getUserPictureData(Picture picture);
    Picture updateUserPicture(Long userId, MultipartFile picture) throws IOException;
    void deleteUserPicture(Long userId);
}
