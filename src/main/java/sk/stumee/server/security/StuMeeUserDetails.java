package sk.stumee.server.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import sk.stumee.server.entities.User;

import java.util.Collection;
import java.util.Collections;

public class StuMeeUserDetails implements UserDetails {
    private Long id;
    private String stuEmail;
    private boolean confirmed;
    private boolean nonDeactivated;

    StuMeeUserDetails(User userEntity) {
        id = userEntity.getId();
        stuEmail = userEntity.getStuEmail();
        confirmed = userEntity.getConfirmed();
        nonDeactivated = !userEntity.getDeactivated();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(Oauth2Config.ROLE_NAME));
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return stuEmail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return nonDeactivated;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return confirmed;
    }
}
