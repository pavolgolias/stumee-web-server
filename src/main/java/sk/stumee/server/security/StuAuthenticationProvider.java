package sk.stumee.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import sk.stumee.server.dao.UserRepository;
import sk.stumee.server.entities.User;
import sk.stumee.server.services.StuAuthService;
import sk.stumee.server.services.StuAuthService.SuccessfulStuAuthResult;

import java.util.Collections;

@Component
public class StuAuthenticationProvider implements AuthenticationProvider {
    private final StuAuthService authService;
    private final UserRepository userRepository;

    @Autowired
    public StuAuthenticationProvider(StuAuthService authService, UserRepository userRepository) {
        this.authService = authService;
        this.userRepository = userRepository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String stuEmail = authentication.getName().trim();
        String stuPassword = authentication.getCredentials().toString();

        if (stuEmail.isEmpty())
            throw new AuthenticationServiceException("Registration email not provided in request");

        User user = userRepository.findByStuEmail(stuEmail);

        if (user == null)
            throw new AuthenticationServiceException("User with email " + stuEmail + " not registered");
        if (!user.getConfirmed())
            throw new AuthenticationServiceException("User account has not been confirmed yet");
        if (user.getDeactivated())
            throw new AuthenticationServiceException("User account is deactivated");

        SuccessfulStuAuthResult result = authService.getAisAuthToken(user.getId().toString(), stuPassword);
        user.setAisAuthToken(result.aisAuthToken);
        user.setAisAuthTokenExpiration(result.aisAuthTokenExpiration);
        user = userRepository.save(user);

        return new UsernamePasswordAuthenticationToken(new StuMeeUserDetails(user), null,
                Collections.singletonList(new SimpleGrantedAuthority(Oauth2Config.ROLE_NAME)));
    }

    @Override
    public boolean supports(Class<?> authClass) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authClass);
    }
}
