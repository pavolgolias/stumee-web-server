package sk.stumee.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import sk.stumee.server.dao.UserRepository;
import sk.stumee.server.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class Oauth2Config {
    static final String ROLE_NAME = "USER";
    static final String ROLE = "ROLE_USER";

    @Bean
    public TokenStore tokenStore(@SuppressWarnings("SpringJavaAutowiringInspection") DataSource dataSource) {
        return new JdbcTokenStore(dataSource);
    }

    @Bean
    public AuthorizationCodeServices authorizationCodeServices(@SuppressWarnings("SpringJavaAutowiringInspection") DataSource dataSource) {
        return new JdbcAuthorizationCodeServices(dataSource);
    }

    @Configuration
    @EnableWebSecurity
    protected static class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Autowired
        private StuAuthenticationProvider authenticationProvider;
        @Autowired
        private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
        @Autowired
        private RestSavedRequestAwareAuthenticationSuccessHandler restSavedRequestAwareAuthenticationSuccessHandler;

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authenticationProvider);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                    .and()
                .exceptionHandling()
                    .authenticationEntryPoint(restAuthenticationEntryPoint)
                    .and()
                .authorizeRequests()
                    .antMatchers("/oauth/authorize").authenticated()
                    .and()
                .csrf().disable()
                .formLogin()
                    .usernameParameter("stuEmail")
                    .passwordParameter("stuPassword")
                    .successHandler(restSavedRequestAwareAuthenticationSuccessHandler)
                    .failureHandler(new SimpleUrlAuthenticationFailureHandler());
        }

        @Component
        static class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
            @Override
            public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            }
        }

        @Component
        static class RestSavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
            private RequestCache requestCache = new HttpSessionRequestCache();

            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                                Authentication authentication) throws ServletException, IOException {
                SavedRequest savedRequest = requestCache.getRequest(request, response);

                if (savedRequest == null) {
                    clearAuthenticationAttributes(request);
                    return;
                }
                String targetUrlParam = getTargetUrlParameter();
                if (isAlwaysUseDefaultTargetUrl() || (targetUrlParam != null && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
                    requestCache.removeRequest(request, response);
                    clearAuthenticationAttributes(request);
                    return;
                }

                clearAuthenticationAttributes(request);
            }

            public void setRequestCache(RequestCache requestCache) {
                this.requestCache = requestCache;
            }
        }
    }

    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
        @SuppressWarnings("SpringJavaAutowiringInspection")
        @Autowired
        private DataSource dataSource;
        @Autowired
        private TokenStore tokenStore;
        @Autowired
        private AuthorizationCodeServices authorizationCodeServices;
        @Autowired
        private AuthenticationManager authenticationManager;
        @Autowired
        private StuMeeUserDetailsService stuMeeUserDetailsService;

        /*@Bean
        public ConsumerTokenServices tokenServices() {
            DefaultTokenServices tokenServices = new DefaultTokenServices();
            tokenServices.setTokenStore(tokenStore);
            return tokenServices;
        }*/

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.authenticationManager(authenticationManager)
                    .authorizationCodeServices(authorizationCodeServices)
                    .tokenStore(tokenStore)
                    .userDetailsService(stuMeeUserDetailsService);
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.jdbc(dataSource);
        }

        @Component
        static class StuMeeUserDetailsService implements UserDetailsService {
            @Autowired
            private UserRepository userRepository;

            @Override
            public UserDetails loadUserByUsername(String stuEmail) throws UsernameNotFoundException {
                User user = userRepository.findByStuEmail(stuEmail);
                if (user == null)
                    throw new UsernameNotFoundException("User with email " + stuEmail + " not found");
                return new StuMeeUserDetails(user);
            }
        }
    }

    @Configuration
    @EnableResourceServer
    protected static class ResourceServerConfig extends ResourceServerConfigurerAdapter {
        @Autowired
        private TokenStore tokenStore;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
            resources.resourceId("stumee-api-resource")
                    .tokenStore(tokenStore);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http
                .requestMatchers()
                    .antMatchers("/api/**")
                    .antMatchers("/oauth/revoke-token")
                    .and()
                .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/api").permitAll()
                    .regexMatchers(HttpMethod.GET, "/api/users/[0-9]+/picture/data").permitAll()
                    .antMatchers(HttpMethod.POST, "/api/users").permitAll()
                    .antMatchers(HttpMethod.POST, "/api/ais/users/search").permitAll()
                    .anyRequest().authenticated();
        }
    }
}
