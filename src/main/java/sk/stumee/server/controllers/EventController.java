package sk.stumee.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sk.stumee.server.dto.autocomplete.KeywordSearchResource;
import sk.stumee.server.dto.event.*;
import sk.stumee.server.dto.event.attachment.*;
import sk.stumee.server.dto.event.comment.CompleteCommentResource;
import sk.stumee.server.dto.event.comment.CompleteCommentResourceAssembler;
import sk.stumee.server.dto.event.comment.CreateCommentResource;
import sk.stumee.server.dto.event.comment.UpdateCommentResource;
import sk.stumee.server.dto.user.ExcerptUserResource;
import sk.stumee.server.dto.user.ExcerptUserResourceAssembler;
import sk.stumee.server.entities.Attachment;
import sk.stumee.server.entities.Comment;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.User;
import sk.stumee.server.security.StuMeeUserDetails;
import sk.stumee.server.services.EventService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@Transactional
@RequestMapping(path = "/api/events")
public class EventController {
    private final EventService eventService;
    private final CompleteEventResourceAssembler completeEventAssembler;
    private final ExcerptEventResourceAssembler excerptEventAssembler;
    private final CompleteCommentResourceAssembler completeCommentAssembler;
    private final CompleteAttachmentResourceAssembler completeAttachmentAssembler;
    private final ExcerptAttachmentResourceAssembler excerptAttachmentAssembler;
    private final ExcerptUserResourceAssembler excerptUserAssembler;

    @Autowired
    public EventController(EventService eventService, CompleteEventResourceAssembler completeEventAssembler,
                           ExcerptEventResourceAssembler excerptEventAssembler, CompleteCommentResourceAssembler completeCommentAssembler,
                           CompleteAttachmentResourceAssembler completeAttachmentAssembler, ExcerptAttachmentResourceAssembler excerptAttachmentAssembler,
                           ExcerptUserResourceAssembler excerptUserAssembler) {
        this.eventService = eventService;
        this.completeEventAssembler = completeEventAssembler;
        this.excerptEventAssembler = excerptEventAssembler;
        this.completeCommentAssembler = completeCommentAssembler;
        this.completeAttachmentAssembler = completeAttachmentAssembler;
        this.excerptAttachmentAssembler = excerptAttachmentAssembler;
        this.excerptUserAssembler = excerptUserAssembler;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptEventResource>> all(Pageable pageable,
                                                                    PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.getEvents(pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/confirmed", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptEventResource>> confirmedFilter(Pageable pageable,
                                                                                PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.getUserAcceptedAccessibleFutureEvents(pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/unconfirmed", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptEventResource>> unconfirmedFilter(Pageable pageable,
                                                                                  PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.getUserUnconfirmedAccessibleEvents(pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/own", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptEventResource>> ownFilter(Pageable pageable,
                                                                          PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.getUserOwnEvents(pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/declined", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptEventResource>> declinedFilter(Pageable pageable,
                                                                               PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.getUserDeclinedAccessibleFutureEvents(pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/old", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptEventResource>> oldFilter(Pageable pageable,
                                                                          PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.getUserAccessiblePastEvents(pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    public ResponseEntity<PagedResources<ExcerptEventResource>> searchEvents(
            @Valid @RequestBody KeywordSearchResource searchResource, Pageable pageable,
            PagedResourcesAssembler<Event> pageAssembler) {
        PagedResources<ExcerptEventResource> resources = pageAssembler.toResource(
                eventService.searchEvents(searchResource.keyword, pageable), excerptEventAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CompleteEventResource> createEvent(@Valid @RequestBody CreateEventResource eventResource) {
        StuMeeUserDetails details = (StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        eventResource.authors.add(details.getId());

        return new ResponseEntity<>(completeEventAssembler.toResource(
                eventService.createEvent(eventResource.toEvent(), eventResource.authors)), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<CompleteEventResource> showEvent(@PathVariable Long eventId) {
        return new ResponseEntity<>(completeEventAssembler.toResource(eventService.getEvent(eventId)), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}", method = RequestMethod.PATCH)
    public ResponseEntity<CompleteEventResource> updateEvent(@PathVariable Long eventId,
                                                             @Valid @RequestBody UpdateEventResource eventResource) {
        return new ResponseEntity<>(completeEventAssembler.toResource(
                eventService.updateEvent(eventId, eventResource.toEvent())), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}", method = RequestMethod.DELETE)
    public ResponseEntity<CompleteEventResource> cancelEvent(@PathVariable Long eventId) {
        return new ResponseEntity<>(completeEventAssembler.toResource(eventService.cancelEvent(eventId)), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/authors", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptUserResource>> eventAuthors(@PathVariable Long eventId,
                                                                            Pageable pageable,
                                                                            PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(
                eventService.getAuthors(eventId, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/authors", method = RequestMethod.POST)
    public ResponseEntity<Void> addEventAuthors(@PathVariable Long eventId, @RequestBody Set<Long> users) {
        eventService.addEventAuthors(eventId, users);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/authors", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteEventAuthors(@PathVariable Long eventId, @RequestBody Set<Long> users) {
        eventService.deleteEventAuthors(eventId, users);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/invited", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptUserResource>> eventInvited(@PathVariable Long eventId,
                                                                            Pageable pageable,
                                                                            PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(
                eventService.getInvitedUsers(eventId, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/invited", method = RequestMethod.POST)
    public ResponseEntity<Void> inviteToEvent(@PathVariable Long eventId, @RequestBody Set<Long> users) {
        eventService.inviteUsersToEvent(eventId, users);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/invitable", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptUserResource>> eventInvitable(
            @PathVariable Long eventId, Pageable pageable, PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(
                eventService.getInvitableUsers(eventId, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/invitable/search", method = RequestMethod.POST)
    public ResponseEntity<PagedResources<ExcerptUserResource>> eventInvitableSearch(
            @PathVariable Long eventId, @Valid @RequestBody KeywordSearchResource searchResource,
            Pageable pageable, PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(
                eventService.searchInvitableUsers(eventId, searchResource.keyword, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/going", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptUserResource>> eventGoing(@PathVariable Long eventId,
                                                                          Pageable pageable,
                                                                          PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(
                eventService.getGoingUsers(eventId, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/going", method = RequestMethod.POST)
    public ResponseEntity<Void> markAsGoingToEvent(@PathVariable Long eventId) {
        StuMeeUserDetails details = (StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        eventService.markUserAsGoingToEvent(eventId, details.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/notGoing", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptUserResource>> eventNotGoing(@PathVariable Long eventId,
                                                                             Pageable pageable,
                                                                             PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(
                eventService.getNotGoingUsers(eventId, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/notGoing", method = RequestMethod.POST)
    public ResponseEntity<Void> markAsNotGoingToEvent(@PathVariable Long eventId) {
        StuMeeUserDetails details = (StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        eventService.markUserAsNotGoingToEvent(eventId, details.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/comments", method = RequestMethod.POST)
    public ResponseEntity<CompleteCommentResource> createEventComment(@PathVariable Long eventId,
                                                                      @RequestBody @Valid CreateCommentResource commentResource) {
        StuMeeUserDetails details = (StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Comment comment = eventService.createEventComment(eventId, details.getId(), commentResource.responseToId, commentResource.toComment());

        return new ResponseEntity<>(completeCommentAssembler.toResource(comment), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{eventId}/comments", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<CompleteCommentResource>> getEventComments(@PathVariable Long eventId,
                                                                                    Pageable pageable,
                                                                                    PagedResourcesAssembler<Comment> pageAssembler) {
        Page<Comment> comments = eventService.getEventComments(eventId, pageable);
        Link selfLink = linkTo(methodOn(EventController.class).getEventComments(eventId, pageable, pageAssembler)).withSelfRel();

        return new ResponseEntity<>(pageAssembler.toResource(comments, completeCommentAssembler, selfLink), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/comments/{commentId}", method = RequestMethod.GET)
    public ResponseEntity<CompleteCommentResource> getEventComment(@PathVariable Long eventId, @PathVariable Long commentId) {
        CompleteCommentResource resource = completeCommentAssembler.toResource(eventService.getEventComment(commentId));
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/comments/{commentId}", method = RequestMethod.PATCH)
    public ResponseEntity<CompleteCommentResource> updateEventComment(@PathVariable Long eventId,
                                                                      @PathVariable Long commentId,
                                                                      @RequestBody @Valid UpdateCommentResource commentResource) {
        return new ResponseEntity<>(completeCommentAssembler.toResource(
                eventService.updateEventComment(commentId, commentResource.toComment())), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/comments/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteEventComment(@PathVariable Long eventId, @PathVariable Long commentId) {
        eventService.deleteEventComment(commentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path = "/{eventId}/attachments", method = RequestMethod.GET)
    public ResponseEntity<Resources<ExcerptAttachmentResource>> getEventAttachments(@PathVariable Long eventId) {
        List<ExcerptAttachmentResource> attachments = excerptAttachmentAssembler.toResources(eventService.getEventAttachments(eventId));
        Resources<ExcerptAttachmentResource> resources = new Resources<>(attachments,
                linkTo(methodOn(EventController.class).getEventAttachments(eventId)).withSelfRel());
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/attachments", method = RequestMethod.POST)
    public ResponseEntity<CompleteAttachmentResource> addEventAttachment(@PathVariable Long eventId,
                                                                         @RequestPart String caption,
                                                                         @RequestPart MultipartFile attachment) throws IOException {
        Attachment a = eventService.createEventAttachment(eventId, attachment, caption);
        return new ResponseEntity<>(completeAttachmentAssembler.toResource(a), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/attachments/{attachmentId}", method = RequestMethod.GET)
    public ResponseEntity<CompleteAttachmentResource> getEventAttachmentInfo(@PathVariable Long eventId,
                                                                             @PathVariable Long attachmentId) {
        Attachment a = eventService.getEventAttachmentInfo(attachmentId);
        return new ResponseEntity<>(completeAttachmentAssembler.toResource(a), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/attachments/{attachmentId}/data", method = RequestMethod.GET)
    public ResponseEntity<FileSystemResource> getEventAttachmentData(@PathVariable Long eventId,
                                                                     @PathVariable Long attachmentId,
                                                                     HttpServletResponse response) {
        Attachment attachment = eventService.getEventAttachmentInfo(attachmentId);
        FileSystemResource resource = eventService.getEventAttachmentData(attachment);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + attachment.getName() + attachment.getExtension() + "\"");
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/attachments/{attachmentId}", method = RequestMethod.PATCH)
    public ResponseEntity<CompleteAttachmentResource> updateEventAttachmentInfo(@PathVariable Long eventId,
                                                                                @PathVariable Long attachmentId,
                                                                                @RequestBody @Valid UpdateAttachmentResource attachmentResource) {
        Attachment a = eventService.updateEventAttachmentInfo(attachmentId, attachmentResource.toAttachment());
        return new ResponseEntity<>(completeAttachmentAssembler.toResource(a), HttpStatus.OK);
    }

    @RequestMapping(path = "/{eventId}/attachments/{attachmentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteEventAttachment(@PathVariable Long eventId, @PathVariable Long attachmentId) {
        eventService.deleteEventAttachment(attachmentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
