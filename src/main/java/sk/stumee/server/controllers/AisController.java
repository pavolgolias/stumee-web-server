package sk.stumee.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.stumee.server.dto.autocomplete.UserRegistrationSuggestKey;
import sk.stumee.server.dto.autocomplete.UserRegistrationSuggestion;
import sk.stumee.server.services.StuAisParserService;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@Transactional
@RequestMapping(path = "/api/ais")
public class AisController {
    private final StuAisParserService aisParserService;

    @Autowired
    public AisController(StuAisParserService aisParserService) {
        this.aisParserService = aisParserService;
    }

    @RequestMapping(path = "/users/search", method = RequestMethod.POST)
    public ResponseEntity<Resources<UserRegistrationSuggestion>> searchAisUsers(@Valid @RequestBody UserRegistrationSuggestKey suggestKey) {
        List<UserRegistrationSuggestion> result = aisParserService.suggestUsers(suggestKey);
        Resources<UserRegistrationSuggestion> resources = new Resources<>(result,
                linkTo(methodOn(AisController.class).searchAisUsers(suggestKey)).withSelfRel());

        return new ResponseEntity<>(resources, HttpStatus.OK);
    }
}
