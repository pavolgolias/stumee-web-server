package sk.stumee.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.stumee.server.dto.oauth.RevokeTokenResource;

import javax.validation.Valid;

@Transactional
@RestController
@RequestMapping(path = "/oauth")
public class OauthController {
    @Autowired
    private ConsumerTokenServices tokenServices;

    @RequestMapping(path = "/revoke-token", method = RequestMethod.POST)
    public ResponseEntity<Void> revokeToken(@RequestBody @Valid RevokeTokenResource tokenResource) {
        SecurityContextHolder.clearContext();
        if (tokenServices.revokeToken(tokenResource.accessToken))
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
