package sk.stumee.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sk.stumee.server.dto.autocomplete.KeywordSearchResource;
import sk.stumee.server.dto.user.*;
import sk.stumee.server.entities.Picture;
import sk.stumee.server.entities.User;
import sk.stumee.server.security.StuMeeUserDetails;
import sk.stumee.server.services.StuAisParserService;
import sk.stumee.server.services.StuAuthService;
import sk.stumee.server.services.StuAuthService.SuccessfulStuAuthResult;
import sk.stumee.server.services.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@Transactional
@RestController
@RequestMapping(path = "/api/users")
public class UserController {
    private final UserService userService;
    private final CompleteUserResourceAssembler completeUserAssembler;
    private final ExcerptUserResourceAssembler excerptUserAssembler;
    private final PictureInfoResourceAssembler pictureInfoAssembler;
    private final StuAuthService stuAuthService;
    private final StuAisParserService aisParserService;

    @Autowired
    public UserController(UserService userService, CompleteUserResourceAssembler completeUserAssembler,
                          ExcerptUserResourceAssembler excerptUserAssembler, PictureInfoResourceAssembler pictureInfoAssembler,
                          StuAuthService stuAuthService, StuAisParserService aisParserService) {
        this.userService = userService;
        this.completeUserAssembler = completeUserAssembler;
        this.excerptUserAssembler = excerptUserAssembler;
        this.pictureInfoAssembler = pictureInfoAssembler;
        this.stuAuthService = stuAuthService;
        this.aisParserService = aisParserService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PagedResources<ExcerptUserResource>> showUsers(Pageable pageable, PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(userService.getUsers(pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    public ResponseEntity<PagedResources<ExcerptUserResource>> searchUsers(@Valid @RequestBody KeywordSearchResource searchResource, Pageable pageable, PagedResourcesAssembler<User> pageAssembler) {
        PagedResources<ExcerptUserResource> resources = pageAssembler.toResource(userService.searchUsers(searchResource.keyword, pageable), excerptUserAssembler);
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CompleteUserResource> createUser(@Valid @RequestBody CreateUserResource userResource) {
        User user = userResource.toUser();

        userService.throwExceptionIfRegistered(user);

        SuccessfulStuAuthResult result = stuAuthService.getAisAuthToken(Long.toString(userResource.id), userResource.stuPassword);
        user.setAisAuthToken(result.aisAuthToken);
        user.setAisAuthTokenExpiration(result.aisAuthTokenExpiration);
        Picture picture = aisParserService.retrieveAndStoreUserPicture(user.getId(), user.getAisAuthToken());

        return new ResponseEntity<>(completeUserAssembler.toResource(userService.saveUser(user, picture)), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/me", method = RequestMethod.GET)
    public ResponseEntity<CompleteUserResource> showMe() {
        StuMeeUserDetails details = (StuMeeUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(completeUserAssembler.toResource(userService.getUser(details.getId())), HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<CompleteUserResource> showUser(@PathVariable Long userId) {
        return new ResponseEntity<>(completeUserAssembler.toResource(userService.getUser(userId)), HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.PATCH)
    public ResponseEntity<CompleteUserResource> updateUser(@PathVariable Long userId, @Valid @RequestBody UpdateUserResource userResource) {

        return new ResponseEntity<>(completeUserAssembler.toResource(userService.updateUser(userId, userResource.toUser())), HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<CompleteUserResource> deactivateUser(@PathVariable Long userId) {
        return new ResponseEntity<>(completeUserAssembler.toResource(userService.deactivateUser(userId)), HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}/picture", method = RequestMethod.GET)
    public ResponseEntity<PictureInfoResource> getPictureInfo(@PathVariable Long userId) {
        return new ResponseEntity<>(pictureInfoAssembler.toResource(userService.getUserPictureInfo(userId)), HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}/picture/data", method = RequestMethod.GET)
    public ResponseEntity<FileSystemResource> getPictureData(@PathVariable Long userId, HttpServletResponse response) {
        Picture picture = userService.getUserPictureInfo(userId);
        FileSystemResource resource = userService.getUserPictureData(picture);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + picture.getName() + picture.getExtension() + "\"");
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}/picture", method = RequestMethod.POST)
    public ResponseEntity<PictureInfoResource> updatePicture(@PathVariable Long userId, @RequestPart("picture") MultipartFile picture) throws IOException {
        return new ResponseEntity<>(pictureInfoAssembler.toResource(userService.updateUserPicture(userId, picture)), HttpStatus.OK);
    }

    @RequestMapping(path = "{userId}/picture", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePicture(@PathVariable Long userId) {
        userService.deleteUserPicture(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
