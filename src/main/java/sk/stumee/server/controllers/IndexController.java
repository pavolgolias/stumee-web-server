package sk.stumee.server.controllers;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Transactional
@RestController
@RequestMapping(path = "/api")
public class IndexController {
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ResourceSupport> get() {
        ResourceSupport indexResource = new ResourceSupport();
        indexResource.add(linkTo(IndexController.class).withSelfRel());
        String loginHref = linkTo(IndexController.class).toString();
        loginHref = loginHref.substring(0, loginHref.lastIndexOf("/api")) + "/login";   //TODO find better way
        indexResource.add(new Link(loginHref, "webapp-login-endpoint"));
        indexResource.add(linkTo(OauthController.class).slash("authorize").withRel("oauth-authorization-endpoint"));
        indexResource.add(linkTo(OauthController.class).slash("token").withRel("oauth-token-endpoint"));
        indexResource.add(linkTo(methodOn(OauthController.class).revokeToken(null)).withRel("oauth-revoke-token-endpoint"));
        indexResource.add(linkTo(UserController.class).withRel("users"));
        indexResource.add(linkTo(methodOn(UserController.class).showMe()).withRel("users-me"));
        indexResource.add(linkTo(methodOn(UserController.class).searchUsers(null, null, null)).withRel("users-search"));
        indexResource.add(linkTo(EventController.class).withRel("events"));
        indexResource.add(linkTo(methodOn(EventController.class).confirmedFilter(null, null)).withRel("events-confirmed"));
        indexResource.add(linkTo(methodOn(EventController.class).unconfirmedFilter(null, null)).withRel("events-unconfirmed"));
        indexResource.add(linkTo(methodOn(EventController.class).ownFilter(null, null)).withRel("events-own"));
        indexResource.add(linkTo(methodOn(EventController.class).declinedFilter(null, null)).withRel("events-declined"));
        indexResource.add(linkTo(methodOn(EventController.class).oldFilter(null, null)).withRel("events-old"));
        indexResource.add(linkTo(methodOn(EventController.class).searchEvents(null, null, null)).withRel("events-search"));
        return new ResponseEntity<>(indexResource, HttpStatus.OK);
    }
}
