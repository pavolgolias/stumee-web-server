package sk.stumee.server.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.dao.base.IndeliblePagingAndSortingRepository;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.User;

@Repository
@Transactional
public interface EventRepository extends IndeliblePagingAndSortingRepository<Event, Long> {
    @Query("SELECT e FROM Event e " +
            "WHERE ((:user MEMBER OF e.invited " +
                    "OR :user MEMBER OF e.going " +
                    "OR :user MEMBER OF e.notGoing " +
                    "OR :user MEMBER OF e.authors) " +
                "AND e.visibility = 'PRIVATE') " +
            "OR e.visibility = 'PUBLIC'")
    Page<Event> findUserAccessibleEvents(@Param("user") User user, Pageable pageable);

    @Query("SELECT e FROM Event e " +
            "WHERE ((:user MEMBER OF e.invited " +
                    "OR :user MEMBER OF e.going " +
                    "OR :user MEMBER OF e.notGoing " +
                    "OR :user MEMBER OF e.authors) " +
                "AND e.visibility = 'PRIVATE' " +
                "AND (LOWER(e.title) LIKE CONCAT('%', LOWER(:keyword), '%') " +
                    "OR LOWER(e.description) LIKE CONCAT('%', LOWER(:keyword), '%'))) " +
            "OR (e.visibility = 'PUBLIC' " +
                "AND (LOWER(e.title) LIKE CONCAT('%', LOWER(:keyword), '%') " +
                    "OR LOWER(e.description) LIKE CONCAT('%', LOWER(:keyword), '%')))")
    Page<Event> findByKeyword(@Param("user") User user, @Param("keyword") String keyword, Pageable pageable);

    @Query("SELECT e FROM Event e " +
            "WHERE :user MEMBER OF e.going " +
            "AND e.canceled = false " +
            "AND e.timestamp >= CURRENT_TIMESTAMP")
    Page<Event> findUserAcceptedAccessibleFutureEvents(@Param("user") User user, Pageable pageable);

    @Query("SELECT e FROM Event e " +
            "WHERE (:user MEMBER OF e.invited " +
                "AND e.visibility = 'PRIVATE' " +
                "AND e.canceled = false " +
                "AND e.timestamp >= CURRENT_TIMESTAMP) " +
            "OR (:user NOT MEMBER OF e.going " +
                "AND :user NOT MEMBER OF e.notGoing " +
                "AND :user NOT MEMBER OF e.authors " +
                "AND e.visibility = 'PUBLIC' " +
                "AND e.canceled = false " +
                "AND e.timestamp >= CURRENT_TIMESTAMP)")
    Page<Event> findUserUnconfirmedAccessibleEvents(@Param("user") User user, Pageable pageable);

    @Query("SELECT e FROM Event e " +
            "WHERE :user MEMBER OF e.authors")
    Page<Event> findUserOwnEvents(@Param("user") User user, Pageable pageable);

    @Query("SELECT e FROM Event e " +
            "WHERE :user MEMBER OF e.notGoing " +
            "AND e.canceled = false " +
            "AND e.timestamp >= CURRENT_TIMESTAMP")
    Page<Event> findUserDeclinedAccessibleFutureEvents(@Param("user") User user, Pageable pageable);

    @Query("SELECT e FROM Event e " +
            "WHERE ((:user MEMBER OF e.invited " +
                    "OR :user MEMBER OF e.going " +
                    "OR :user MEMBER OF e.notGoing) " +
                "AND e.visibility = 'PRIVATE' " +
                "AND e.timestamp < CURRENT_TIMESTAMP) " +
            "OR (e.visibility = 'PUBLIC'  " +
                "AND e.timestamp < CURRENT_TIMESTAMP)")
    Page<Event> findUserAccessiblePastEvents(@Param("user") User user, Pageable pageable);
}
