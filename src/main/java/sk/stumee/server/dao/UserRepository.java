package sk.stumee.server.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.dao.base.IndeliblePagingAndSortingRepository;
import sk.stumee.server.entities.Event;
import sk.stumee.server.entities.User;

@Repository
@Transactional
public interface UserRepository extends IndeliblePagingAndSortingRepository<User, Long> {
    Page<User> findByDeactivatedFalse(Pageable pageable);

    User findByUsername(String username);

    User findByStuEmail(String stuEmail);

    @Query("SELECT u FROM User u JOIN u.authoredEvents e WHERE e = :event")
    Page<User> findEventAuthors(@Param("event") Event event, Pageable pageable);

    @Query("SELECT u FROM User u JOIN u.invitedToEvents e WHERE e = :event")
    Page<User> findEventInvitedUsers(@Param("event") Event event, Pageable pageable);

    @Query("SELECT u FROM User u JOIN u.goingToEvents e WHERE e = :event")
    Page<User> findEventGoingUsers(@Param("event") Event event, Pageable pageable);

    @Query("SELECT u FROM User u JOIN u.notGoingToEvents e WHERE e = :event")
    Page<User> findEventNotGoingUsers(@Param("event") Event event, Pageable pageable);

    @Query("SELECT u FROM User u " +
            "WHERE u.deactivated = false " +
            "AND (LOWER(u.username) LIKE CONCAT('%', LOWER(:keyword), '%') " +
                "OR LOWER(u.name) LIKE CONCAT('%', LOWER(:keyword), '%') " +
                "OR LOWER(u.surname) LIKE CONCAT('%', LOWER(:keyword), '%'))")
    Page<User> findByKeyword(@Param("keyword") String keyword, Pageable pageable);

    @Query("SELECT u FROM User u " +
            "WHERE :event NOT MEMBER OF u.authoredEvents " +
            "AND :event NOT MEMBER OF u.invitedToEvents " +
            "AND :event NOT MEMBER OF u.goingToEvents " +
            "AND :event NOT MEMBER OF u.notGoingToEvents " +
            "AND u.deactivated = false")
    Page<User> findEventInvitableUsers(@Param("event") Event event, Pageable pageable);

    @Query("SELECT u FROM User u " +
            "WHERE :event NOT MEMBER OF u.authoredEvents " +
            "AND :event NOT MEMBER OF u.invitedToEvents " +
            "AND :event NOT MEMBER OF u.goingToEvents " +
            "AND :event NOT MEMBER OF u.notGoingToEvents " +
            "AND u.deactivated = false " +
            "AND (LOWER(u.username) LIKE CONCAT('%', LOWER(:keyword), '%') " +
                "OR LOWER(u.name) LIKE CONCAT('%', LOWER(:keyword), '%') " +
                "OR LOWER(u.surname) LIKE CONCAT('%', LOWER(:keyword), '%'))")
    Page<User> findEventInvitableUsersByKeyword(@Param("event") Event event, @Param("keyword") String keyword, Pageable pageable);
}
