package sk.stumee.server.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.entities.Attachment;
import sk.stumee.server.entities.Event;

@Repository
@Transactional
public interface AttachmentRepository extends CrudRepository<Attachment, Long> {
    Iterable<Attachment> findAllByEvent(Event event);
}
