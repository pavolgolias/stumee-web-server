package sk.stumee.server.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sk.stumee.server.entities.Comment;
import sk.stumee.server.entities.Event;

@Repository
@Transactional
public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {
    Page<Comment> findAllByEvent(Event event, Pageable pageable);
}
