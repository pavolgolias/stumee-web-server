package sk.stumee.server.entities;

public enum EventVisibility {
    PUBLIC,
    PRIVATE
}
