package sk.stumee.server.entities;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "events")
public class Event implements Identifiable<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    @NotBlank
    private String title;

    @Column(name = "description", nullable = false)
    @NotBlank
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, updatable = false)
//    @Past
    @NotNull
    private Date created = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", nullable = false)
    @Future
    @NotNull
    private Date timestamp;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private EventType type;

    @Column(name = "visibility", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private EventVisibility visibility;

    @Column(name = "location", nullable = false)
    @NotBlank
    private String location;

    @Column(name = "canceled", nullable = false, columnDefinition = "TINYINT(1)")   //TODO mysql dependent column definition
    @NotNull
    private Boolean canceled = false;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Attachment> attachments;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Comment> comments;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_authors")
    @NotEmpty
    private Set<User> authors;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_invited")
    private Set<User> invited;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_going")
    private Set<User> going;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "events_not_going")
    private Set<User> notGoing;

    public Event() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public EventVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(EventVisibility visibility) {
        this.visibility = visibility;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public Set<User> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<User> authors) {
        this.authors = authors;
    }

    public Set<User> getInvited() {
        return invited;
    }

    public void setInvited(Set<User> invited) {
        this.invited = invited;
    }

    public Set<User> getGoing() {
        return going;
    }

    public void setGoing(Set<User> going) {
        this.going = going;
    }

    public Set<User> getNotGoing() {
        return notGoing;
    }

    public void setNotGoing(Set<User> notGoing) {
        this.notGoing = notGoing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return title.equals(event.title)
                && created.equals(event.created)
                && timestamp.equals(event.timestamp)
                && location.equals(event.location);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + created.hashCode();
        result = 31 * result + timestamp.hashCode();
        result = 31 * result + location.hashCode();
        return result;
    }
}
