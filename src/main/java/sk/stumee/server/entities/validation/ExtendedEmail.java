package sk.stumee.server.entities.validation;

import org.hibernate.validator.constraints.Email;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

@Email
@Pattern(regexp = ".+@.+\\..+")
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface ExtendedEmail {
    String message() default "Please provide valid email address.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
