package sk.stumee.server.entities;

public enum EventType {
    STUDY,
    FUN,
    SPORT,
    FOOD,
    FREE_TIME,
    WORK,
    OTHER
}
