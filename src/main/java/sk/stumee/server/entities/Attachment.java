package sk.stumee.server.entities;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

@Entity
@Table(name = "events_attachments")
public class Attachment implements Identifiable<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    @NotBlank
    private String name;

    @Column(name = "extension", nullable = false)
    @NotNull
    private String extension;

    @Column(name = "caption")
    private String caption;

    @Column(name = "type", nullable = false)
    @NotBlank
    private String contentType;

    @Column(name = "size", nullable = false)
    @NotNull
    private Long size;

    @Column(name = "created", nullable = false)
    @NotNull
    @Past
    private Date created = new Date();

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "event_id", nullable = false, updatable = false)
    @NotNull
    private Event event;

    public Attachment() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attachment that = (Attachment) o;

        return name.equals(that.name)
                && event.equals(that.event)
                && size.equals(that.size)
                && created.equals(that.created);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + event.hashCode();
        result = 31 * result + size.hashCode();
        result = 31 * result + created.hashCode();
        return result;
    }
}
