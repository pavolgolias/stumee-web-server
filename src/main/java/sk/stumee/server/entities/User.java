package sk.stumee.server.entities;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.hateoas.Identifiable;
import sk.stumee.server.entities.validation.ExtendedEmail;
import sk.stumee.server.entities.validation.StuEmail;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "users")
public class User implements Identifiable<Long> {
    @Id
    @Column(name = "ais_id", nullable = false, updatable = false)
    @NotNull
    private Long id;

    @Column(name = "username", nullable = false, unique = true)
    @NotNull
    @Pattern(regexp = "^[\\w\\.]{3,30}$")
    private String username;

    @Column(name = "stu_email", nullable = false, unique = true)
    @NotNull
    @StuEmail
    private String stuEmail;

    @Column(name = "secondary_email")
    @ExtendedEmail
    private String secondaryEmail;

    @Column(name = "confirmed", nullable = false, columnDefinition = "TINYINT(1)")   //TODO mysql dependent column definition
    private Boolean confirmed = true;

    @Column(name = "deactivated", nullable = false, columnDefinition = "TINYINT(1)")   //TODO mysql dependent column definition
    private Boolean deactivated = false;

    @Column(name = "name", nullable = false)
    @NotBlank
    private String name;

    @Column(name = "surname", nullable = false)
    @NotBlank
    private String surname;

    @Column(name = "description")
    private String description;

    @Column(name = "ais_auth_token")
    private String aisAuthToken;

    @Column(name = "ais_auth_token_expiration")
    @Future
    private Date aisAuthTokenExpiration;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Picture picture;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    private Set<Comment> comments;

    @ManyToMany(mappedBy = "authors", fetch = FetchType.LAZY)
    private Set<Event> authoredEvents;

    @ManyToMany(mappedBy = "invited", fetch = FetchType.LAZY)
    private Set<Event> invitedToEvents;

    @ManyToMany(mappedBy = "going", fetch = FetchType.LAZY)
    private Set<Event> goingToEvents;

    @ManyToMany(mappedBy = "notGoing", fetch = FetchType.LAZY)
    private Set<Event> notGoingToEvents;

    public User() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAisAuthToken() {
        return aisAuthToken;
    }

    public void setAisAuthToken(String aisAuthToken) {
        this.aisAuthToken = aisAuthToken;
    }

    public Date getAisAuthTokenExpiration() {
        return aisAuthTokenExpiration;
    }

    public void setAisAuthTokenExpiration(Date aisAuthTokenExpiration) {
        this.aisAuthTokenExpiration = aisAuthTokenExpiration;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStuEmail() {
        return stuEmail;
    }

    public void setStuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Boolean getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(Boolean deactivated) {
        this.deactivated = deactivated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username)
                && stuEmail.equals(user.stuEmail);

    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + stuEmail.hashCode();
        return result;
    }
}
