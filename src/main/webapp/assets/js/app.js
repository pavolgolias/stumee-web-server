'use strict';


var stuMeeApp = angular.module('stuMeeApp', [
    'ngRoute',
    'stuMeeAppControllers',
    'ngMaterial',
    'stuMeeAppServices'
]);

stuMeeApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/meetings.html',
            controller: 'meetingsController'
        }).
        when('/login', {
            templateUrl: 'partials/login.html',
            controller: 'loginController'
        }).
        when('/profile', {
            templateUrl: 'partials/profile.html',
            controller: 'profileController'
        }).
        when('/register', {
            templateUrl: 'partials/register.html',
            controller: 'registerController'
        }).
        otherwise({
            redirectTo: '/'
        })

    }]);


stuMeeApp.config(function($mdThemingProvider) {
        // Extend the red theme with a few different colors
        var neonGreenMap = $mdThemingProvider.extendPalette('green', {
            '500': '00A99D'
        });
        // Register the new color palette map with the name <code>neonRed</code>
        $mdThemingProvider.definePalette('neonGreen', neonGreenMap);
        // Use that theme for the primary intentions
        $mdThemingProvider.theme('default')
            .primaryPalette('neonGreen')
    });

stuMeeApp.run(function($rootScope,$resource) {


    // $rootScope.SERVER_BASE_URL = "http://localhost:8080";
    $rootScope.SERVER_BASE_URL = "http://stumee-andrejkosar.rhcloud.com";

    var userResource = $resource($rootScope.SERVER_BASE_URL+"/api/users/me");
    var user = userResource.get(null, function () {
        $rootScope.user = user;

        var userFotoResource = $resource($rootScope.user.links.picture);
        var userFoto = userFotoResource.get(null, function () {
            if(userFoto.hasOwnProperty('name')){
                $rootScope.user.canDelete = true;
                $rootScope.user.imageLink = user.links.pictureData;
            }
            else {
                $rootScope.user.canDelete = false;
                $rootScope.user.imageLink = "";
            }
        });
    });
    
    $rootScope.verifyPerson = {};

    $rootScope.MAX_INT = 2147483647;


    $rootScope.getTypeOfMeeting = function (type){

        switch(type){
            case "STUDY":
                return {
                    'icon': "school",
                    'name': "Study"
                };

            case "SPORT":
                return{
                    'icon': "fitness_center",
                    'name': "Sport"
                };

            case "FUN":
                return {
                    'icon': "sentiment_very_satisfied",
                    'name': "Fun"
                };

            case "FOOD":
                return {
                    'icon': "restaurant",
                    'name': "Food"
                };

            case "FREE_TIME":
                return {
                    'icon': "local_cafe",
                    'name': "Free time"
                };

            case "WORK":
                return {
                    'icon': "work",
                    'name': "Work"
                };

            case "OTHER":
                return {
                    'icon': "more_horiz",
                    'name': "Other"
                };

        }
    };

    $rootScope.typesOfMeeting = ['STUDY','SPORT','FUN','FOOD','FREE_TIME','WORK','OTHER'];

    $rootScope.getActualFilter = function (type) {
        switch(type){
            case 1:
                return{
                    label: "UNCONFIRMED EVENTS"
                };
            case 2:
                return{
                    label: "MY EVENTS"
                };
            case 3:
                return{
                    label: "DECLINED EVENTS"
                };
            case 4:
                return{
                    label: "OLD EVENTS"
                };
            default:
                return{
                    label: "EVENTS"
                };
        }
    };

    $rootScope.actualFilter = 0;

    $rootScope.getTypeOfVisibility = function (type){

        switch(type){
            case "PUBLIC":
                return {
                    label: "Public event",
                    class: ["md-public",'md-public-background'],
                    icon: "people"
                };

            case "PRIVATE":
                return{
                    label: "Private event",
                    class: ["md-private",'md-private-background'],
                    icon: "person"
                };
        }
    };

    $rootScope.setDateFormat = function(date, isWithTime){
        var myDate = new Date(date);
        if(isWithTime){
            return myDate.getDate()+"."+(myDate.getMonth()+1)+"."+myDate.getFullYear()+' - '+(("0" + myDate.getHours()).slice(-2))+':'+((myDate.getMinutes()<10?'0':'') + myDate.getMinutes());
        }
        return myDate.getDate()+"."+(myDate.getMonth()+1)+"."+myDate.getFullYear();
    };
    
    
});