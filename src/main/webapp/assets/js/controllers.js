'use strict';


var stuMeeAppControllers = angular.module('stuMeeAppControllers', ['ngMaterial','ngResource', "hateoas"]);

stuMeeAppControllers.factory('myHttpResponseInterceptor',['$q','$location',function($q,$location){
    return {
        'responseError': function(rejection) {
            if(rejection.status === 401 && $location.path() != '/register'){
                $location.path('/login');
            }
            return $q.reject(rejection);
        }
    };
}]);


angular.module('stuMeeAppControllers').directive('ngReallyClick', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.ngReallyMessage;
                if (message && confirm(message)) {
                    scope.$apply(attrs.ngReallyClick);
                }
            });
        }
    }
}]);


angular.module('stuMeeAppControllers')
    .factory("fileReader", function($q, $log) {
        var onLoad = function(reader, deferred, scope) {
            return function() {
                scope.$apply(function() {
                    deferred.resolve(reader.result);
                });
            };
        };

        var onError = function(reader, deferred, scope) {
            return function() {
                scope.$apply(function() {
                    deferred.reject(reader.result);
                });
            };
        };

        var onProgress = function(reader, scope) {
            return function(event) {
                scope.$broadcast("fileProgress", {
                    total: event.total,
                    loaded: event.loaded
                });
            };
        };

        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };

        var readAsDataURL = function(file, scope) {
            var deferred = $q.defer();

            var reader = getReader(deferred, scope);
            reader.readAsDataURL(file);

            return deferred.promise;
        };

        return {
            readAsDataUrl: readAsDataURL
        };
    });


angular.module('stuMeeAppControllers')
    .directive("ngFileSelect", function(fileReader, $timeout, $rootScope) {
        return {
            scope: {
                ngModel: '='
            },
            link: function($scope, el) {
                function getFile(file) {
                    fileReader.readAsDataUrl(file, $scope)
                        .then(function(result) {
                            $timeout(function() {
                                $scope.ngModel = result;
                            });
                        });
                }
                el.bind("change", function(e) {
                    var file = (e.srcElement || e.target).files[0];
                    if(parseFloat(file.size/1024/1024)>4){
                        alert("Max photo size is 4 MB.");
                        return false;
                    }
                    else{
                        $rootScope.editPhoto = true;
                    }
                    getFile(file);
                });
            }
        };
    });



stuMeeAppControllers.config(['$httpProvider', function($httpProvider) {

    $httpProvider.interceptors.push('myHttpResponseInterceptor');

    var path = window.location.href;
    var token = path.substr(path.indexOf('#access_token=')).split('&')[0].split('=')[1];
    if(token != null){
        localStorage.setItem('accessToken', JSON.stringify(token));
    }
    //console.log(JSON.parse(localStorage.getItem('accessToken')));
    $httpProvider.defaults.headers.patch = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization':' Bearer '+JSON.parse(localStorage.getItem('accessToken'))
    };

    $httpProvider.defaults.headers.get = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization':' Bearer '+JSON.parse(localStorage.getItem('accessToken'))
    };

    $httpProvider.defaults.headers.post = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization':' Bearer '+JSON.parse(localStorage.getItem('accessToken'))
    };

    $httpProvider.defaults.headers.delete = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization':' Bearer '+JSON.parse(localStorage.getItem('accessToken'))
    };
}]);

stuMeeAppControllers.config(function (HateoasInterceptorProvider) {
    HateoasInterceptorProvider.transformAllResponses();
});


stuMeeAppControllers.controller('meetingsController',['$scope','$mdDialog', '$mdMedia','$rootScope','$location','$mdToast','$http','$resource',
    function ( $scope, $mdDialog, $mdMedia, $rootScope, $location, $mdToast, $http ,$resource) {

    var DialogController = ['$scope', '$mdDialog','$rootScope','hideCreatingMeeting','event','$mdToast',
        function( $scope, $mdDialog, $rootScope, hideCreatingMeeting, event, $mdToast ) {

            $scope.event = event;

            $scope.hideCreatingMeeting = hideCreatingMeeting;

            $scope.event.hours = showHours($scope.event.timestamp);

            $scope.event.minutes = showMinutes($scope.event.timestamp);

            $scope.isEdit = false;

            $scope.showEditEvent = true;


            if(hideCreatingMeeting){

                $scope.acceptInvitation = function () {
                    var goResource = $resource($scope.event.links.going);
                    goResource.save();

                    if($rootScope.user.isInvited){
                        angular.forEach($scope.peopleInvited, function (item,key) {
                            if(item.id == $rootScope.user.id){
                                $scope.peopleInvited.splice(key,1);
                            }
                        });
                    }

                    angular.forEach($scope.notGoing, function (item,key) {
                        if(item.id == $rootScope.user.id){
                            $scope.notGoing.splice(key,1);
                        }
                    });
                    $scope.going.push($rootScope.user);

                    $rootScope.user.isGoing = true;
                    $rootScope.user.isNotGoing = false;
                    $rootScope.user.isInvited = false;
                };

                $scope.declineInvitation = function () {
                    var notGoResource = $resource($scope.event.links.notGoing);
                    notGoResource.save();

                    if($rootScope.user.isInvited){
                        angular.forEach($scope.peopleInvited, function (item,key) {
                            if(item.id == $rootScope.user.id){
                                $scope.peopleInvited.splice(key,1);
                            }
                        });
                    }

                    angular.forEach($scope.going, function (item,key) {
                        if(item.id == $rootScope.user.id){
                            $scope.going.splice(key,1);
                        }
                    });
                    $scope.notGoing.push($rootScope.user);


                    $rootScope.user.isGoing = false;
                    $rootScope.user.isNotGoing = true;
                    $rootScope.user.isInvited = false;
                };
                
                
                $scope.isEdited = false;

                var goingResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/going?size="+$rootScope.MAX_INT);
                var goingPeople = goingResource.get({ eventId: $scope.event.id }, function () {
                    $scope.going = goingPeople.content;
                    var isFinded = false;
                    angular.forEach($scope.going, function (item) {
                        if(item.id == $rootScope.user.id){
                            isFinded = true;
                        }
                    });
                    $rootScope.user.isGoing = isFinded;
                });


                var notGoingResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/notGoing?size="+$rootScope.MAX_INT);
                var notGoingPeople = notGoingResource.get({ eventId: $scope.event.id }, function () {
                    $scope.notGoing = notGoingPeople.content;
                    var isFinded = false;
                    angular.forEach($scope.notGoing, function (item) {
                        if(item.id == $rootScope.user.id){
                            isFinded = true;
                        }
                    });
                    $rootScope.user.isNotGoing = isFinded;

                });


                var invitedResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/invited?size="+$rootScope.MAX_INT);
                var invitedPeople = invitedResource.get({ eventId: $scope.event.id }, function () {
                    $scope.peopleInvited = invitedPeople.content;
                    var isFinded = false;
                    angular.forEach($scope.peopleInvited, function (item) {
                        if(item.id == $rootScope.user.id){
                            isFinded = true;
                        }
                    });
                    $rootScope.user.isInvited = isFinded;
                });

                var authorsResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/authors?size="+$rootScope.MAX_INT);
                var authors = authorsResource.get({ eventId: $scope.event.id }, function () {
                    $scope.authors = authors.content;
                    $scope.showEditEvent = true;
                    var isFinded = false;
                    angular.forEach($scope.authors, function (item) {
                        if(item.id == $rootScope.user.id){
                            $scope.showEditEvent = false;
                            isFinded = true;
                            return false;
                        }
                    });
                    $rootScope.user.isAuthor = isFinded;
                });

                $scope.allContacts = $scope.peopleInvited;
                $scope.contacts = [];

                /**
                 * Search for contacts.
                 */
                $scope.querySearch = function(query) {
                    var results = query ?
                        $scope.allContacts.filter($scope.createFilterFor(query)) : [];
                    return results;
                };
                /**
                 * Create filter function for a query string
                 */
                $scope.createFilterFor = function(query) {
                    var lowercaseQuery = angular.lowercase(query);
                    return function filterFn(contact) {
                        return (contact.surname.toLowerCase().indexOf(lowercaseQuery) != -1);
                    };
                };

                var commentsResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/comments?sort=timestamp,desc&size="+$rootScope.MAX_INT);
                var comments = commentsResource.get({ eventId: $scope.event.id }, function () {
                    $scope.comments = comments.content;

                });

                $scope.newComment = {
                    content: ""
                };

                $scope.addComment = function () {

                    var addCommentResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/comments");
                    addCommentResource.save({ eventId: $scope.event.id },$scope.newComment,function (comment) {
                        $scope.comments.unshift(comment);
                        $scope.newComment = '';
                    });

                }

            }
            else{

                $scope.newEvent = {
                    title: "",
                    type: "",
                    location: "",
                    timestamp: "",
                    visibility: "PUBLIC",
                    description: "",
                    tempDateForEvent: "",
                    hours: "",
                    minutes: ""
                };

                $scope.typesOfMeeting = $rootScope.typesOfMeeting.map(
                    function (typeOfEvent) {
                        return {text: typeOfEvent};
                    });

                $scope.defaultSelectType = 'STUDY';

                $scope.createEditEvent = function() {

                    if($scope.newEvent.tempDateForEvent == ''){
                        $scope.showSimpleToast('date');

                        $scope.eventCreate['date'].$setValidity('error',false);
                    }
                    else{
                        $scope.newEvent.timestamp = $scope.newEvent.tempDateForEvent.getTime();
                        if($scope.newEvent.tempDateForEvent< new Date()){
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent( 'You must set future date' )
                                    .position($scope.getToastPosition())
                                    .hideDelay(3000)

                            );
                            $scope.eventCreate['date'].$setValidity('error',false);

                        }
                        else if( eventHasSetParameters($scope.newEvent) ){
                            $scope.newEvent.tempDateForEvent.setHours($scope.newEvent.hours);
                            $scope.newEvent.tempDateForEvent.setMinutes($scope.newEvent.minutes);
                            $scope.newEvent.timestamp = $scope.newEvent.tempDateForEvent.getTime();

                            var createEventResource = $resource($rootScope.SERVER_BASE_URL+"/api/events");
                            $scope.createdEvent = null;
                            createEventResource.save(null,$scope.newEvent,function(createdEvent) {

                                if($rootScope.actualFilter == 2){
                                    $rootScope.events.push(createdEvent);
                                }

                                $scope.createdEvent = createdEvent;

                                $scope.invitedPeopleIDs = [];
                                angular.forEach($scope.selectedPeople, function (item) {
                                    $scope.invitedPeopleIDs.push(item.id);
                                });

                                if($scope.invitedPeopleIDs.length>0){
                                    var invitePoepleResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/invited");
                                    invitePoepleResource.save({ eventId: $scope.createdEvent.id },$scope.invitedPeopleIDs);
                                }

                            });

                            $mdDialog.hide();

                        }
                    }


                };

                $scope.selectedItem = null;
                $scope.searchText = null;
                $scope.selectedPeople = [];

                var usersResource = $resource($rootScope.SERVER_BASE_URL+"/api/users?size="+$rootScope.MAX_INT);

                var people = usersResource.get(null, function () {
                    $scope.people = [];
                    angular.forEach(people.content, function (item) {
                        if(item.id != $rootScope.user.id){
                            $scope.people.push(item);
                        }
                    });

                    $scope.querySearch = function(query) {
                        var results = query ?
                            $scope.people.filter($scope.createFilterFor(query)) : [];
                        return results;
                    };

                    $scope.createFilterFor = function(query) {
                        var lowercaseQuery = angular.lowercase(query);
                        return function filterFn(contact) {
                            return (contact.surname.toLowerCase().indexOf(lowercaseQuery) != -1) ||
                                (contact.name.toLowerCase().indexOf(lowercaseQuery) != -1) ||
                                (contact.username.toLowerCase().indexOf(lowercaseQuery) != -1);
                        };
                    };
                });


                $scope.transformChip = function (chip) {
                    if (angular.isObject(chip)) {
                        return chip;
                    }
                    return { name: chip, type: 'new' }
                };

            }
            /*****************END CREATE EVENT********************/


            $scope.setToEditMeeting = function () {

                $scope.isEdited = true;

                $scope.newEvent = $scope.event;
                $scope.newEvent.tempDateForEvent = new Date($scope.event.timestamp);

                $scope.typesOfMeeting = $rootScope.typesOfMeeting.map(
                    function (typeOfEvent) {
                        return {text: typeOfEvent};
                    });
                $scope.isEdit = true;
                $scope.hideCreatingMeeting = false;

                $scope.defaultSelectType = $scope.newEvent.type;

                $scope.createEditEvent = function() {

                    delete $scope.newEvent.canceled;
                    delete $scope.newEvent.links;

                    $scope.newEvent.timestamp = $scope.newEvent.tempDateForEvent.getTime();
                    if($scope.newEvent.tempDateForEvent< new Date()){
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent( 'You must set future date' )
                                .position($scope.getToastPosition())
                                .hideDelay(3000)

                        );
                        $scope.eventCreate['date'].$setValidity('error',false);

                    }
                    else if( eventHasSetParameters($scope.newEvent) ){
                        $scope.newEvent.tempDateForEvent.setHours($scope.newEvent.hours);
                        $scope.newEvent.tempDateForEvent.setMinutes($scope.newEvent.minutes);
                        $scope.newEvent.timestamp = $scope.newEvent.tempDateForEvent.getTime();

                        if($scope.isEdit){

                            var eventUpdateResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:userId", null,
                                {
                                    'update': { method:'PATCH' }
                                });

                            eventUpdateResource.update({ userId: $scope.newEvent.id },$scope.newEvent,function (editedEvent) {

                                $scope.invitedPeopleIDs = [];
                                angular.forEach($scope.selectedPeople, function (item) {
                                    $scope.invitedPeopleIDs.push(item.id);
                                });

                                if($scope.invitedPeopleIDs.length>0){
                                    var invitePoepleResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId/invited");
                                    invitePoepleResource.save({ eventId: editedEvent.id },$scope.invitedPeopleIDs);
                                }
                            });

                            $mdDialog.hide();

                        }

                    }

                };

                $scope.selectedItem = null;
                $scope.searchText = null;
                $scope.selectedPeople = [];

                var usersResource = $resource($scope.newEvent.links.invitable+"?size="+$rootScope.MAX_INT);

                var people = usersResource.get(null, function () {
                    $scope.people = people.content;

                    $scope.querySearch = function(query) {
                        var results = query ?
                            $scope.people.filter($scope.createFilterFor(query)) : [];
                        return results;
                    };

                    $scope.createFilterFor = function(query) {
                        var lowercaseQuery = angular.lowercase(query);
                        return function filterFn(contact) {
                            return (contact.surname.toLowerCase().indexOf(lowercaseQuery) != -1) ||
                                (contact.name.toLowerCase().indexOf(lowercaseQuery) != -1) ||
                                (contact.username.toLowerCase().indexOf(lowercaseQuery) != -1);
                        };
                    };
                });


                $scope.transformChip = function (chip) {
                    if (angular.isObject(chip)) {
                        return chip;
                    }
                    return { name: chip, type: 'new' }
                };

            };

            $scope.getTypeOfMeeting = function(item){
                return $rootScope.getTypeOfMeeting(item);
            };

            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };

            $scope.toastPosition = angular.extend({},last);

            $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function(pos) { return $scope.toastPosition[pos]; })
                    .join(' ');
            };

            function sanitizePosition() {
                var current = $scope.toastPosition;
                if ( current.bottom && last.top ) current.top = false;
                if ( current.top && last.bottom ) current.bottom = false;
                if ( current.right && last.left ) current.left = false;
                if ( current.left && last.right ) current.right = false;
                last = angular.extend({},current);
            }

            $scope.showSimpleToast = function( text ) {

                $mdToast.show(
                    $mdToast.simple()
                        .textContent( 'You must set '+ text )
                        .position($scope.getToastPosition())
                        .hideDelay(3000)

                );
            };

            function eventHasSetParameters( obj ) {
                for (var property in obj) {
                    if (obj.hasOwnProperty(property)) {
                       if( obj[property]== "" || (typeof obj[property] === 'undefined')){
                           $scope.showSimpleToast(property);
                           $scope.eventCreate[property].$setValidity('error',false);
                           return false;
                       }

                    }
                }

                return true;
            }


            function showHours(timestamp) {
                var myDate = new Date(timestamp);

                return (("0" + myDate.getHours()).slice(-2));
            }

            function showMinutes (timestamp) {
                var myDate = new Date(timestamp);

                return ((myDate.getMinutes()<10?'0':'') + myDate.getMinutes());
            }

    }];

    /**************************END DIALOG CONTROLLER***************************/
        
        if($rootScope.actualFilter == 1){
            var unconfirmedResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/unconfirmed?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var unconfirmed = unconfirmedResource.get(null, function () {
                $rootScope.events = unconfirmed.content;
            });
            $scope.eventsLabel = 'UNCONFIRMED EVENTS';
            $rootScope.actualFilter = 1;
        }
        else if($rootScope.actualFilter == 2){
            var ownResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/own?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var own = ownResource.get(null, function () {
                $rootScope.events = own.content;
            });
            $scope.eventsLabel = 'MY EVENTS';
            $rootScope.actualFilter = 2;
        }
        else if($rootScope.actualFilter == 3){
            var desclinedResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/declined?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var declined = desclinedResource.get(null, function () {
                $rootScope.events = declined.content;
            });
            $scope.eventsLabel = 'DECLINED EVENTS';
            $rootScope.actualFilter = 3;
        }
        else if($rootScope.actualFilter == 4){
            var oldResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/old?sort=timestamp,desc&size="+$rootScope.MAX_INT);
            var old = oldResource.get(null, function () {
                $rootScope.events = old.content;
            });
            $scope.eventsLabel = 'OLD EVENTS';
            $rootScope.actualFilter = 4;
        }
        else{
            var homeResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/confirmed?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var home = homeResource.get(null, function () {
                $rootScope.events = home.content;
            });
            $scope.eventsLabel = 'EVENTS';
            $rootScope.actualFilter = 0;
        }

        $scope.getHome = function () {

            var homeResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/confirmed?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var home = homeResource.get(null, function () {
                $rootScope.events = home.content;
            });
            $scope.eventsLabel = 'EVENTS';
            $rootScope.actualFilter = 0;
            $location.path("/");
        };

        $scope.getUnconfirmed = function () {

            var unconfirmedResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/unconfirmed?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var unconfirmed = unconfirmedResource.get(null, function () {
                $rootScope.events = unconfirmed.content;
            });
            $scope.eventsLabel = 'UNCONFIRMED EVENTS';
            $rootScope.actualFilter = 1;
            $location.path("/");
        };

        $scope.getOwn = function () {

            var ownResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/own?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var own = ownResource.get(null, function () {
                $rootScope.events = own.content;
            });
            $scope.eventsLabel = 'MY EVENTS';
            $rootScope.actualFilter = 2;
            $location.path("/");
        };

        $scope.getDeclined = function () {

            var desclinedResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/declined?sort=timestamp,asc&size="+$rootScope.MAX_INT);
            var declined = desclinedResource.get(null, function () {
                $rootScope.events = declined.content;
            });
            $scope.eventsLabel = 'DECLINED EVENTS';
            $rootScope.actualFilter = 3;
            $location.path("/");
        };

        $scope.getOld = function () {

            var oldResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/old?sort=timestamp,desc&size="+$rootScope.MAX_INT);
            var old = oldResource.get(null, function () {
                $rootScope.events = old.content;
            });
            $scope.eventsLabel = 'OLD EVENTS';
            $rootScope.actualFilter = 4;
            $location.path("/");
        };

        $rootScope.backgroundValue = "";

        $scope.setDateFormat = function(date){
            var myDate = new Date(date);
            return myDate.getDate()+"."+(myDate.getMonth()+1)+"."+myDate.getFullYear();
        };
    
        $scope.setDescription = function( fullDesc ){
    
            var descriptionShort = "";
            if( fullDesc.length > 20){
                descriptionShort = fullDesc.substring(0,20)+"...";
            }
            else{
                descriptionShort = fullDesc;
            }
    
            return descriptionShort;
        };
    
        $scope.status = '  ';
        $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
        $scope.myAnswer = true;

        $scope.showEvent = function(ev,eventId,hideCreatingMeeting) {
    
            if(hideCreatingMeeting){
    
                var eventResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/:eventId?size="+$rootScope.MAX_INT);
    
                var event = eventResource.get({ eventId: eventId }, function () {
                    $scope.event = event;
                    $scope.event.myDate = new Date($scope.event.timestamp);
    
                    $scope.myAnswer = false;
                    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'partials/meeting.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        fullscreen: useFullScreen,
                        onRemoving: setAnswer,
                        locals: {
                            event: $scope.event,
                            hideCreatingMeeting: hideCreatingMeeting
                        }
                    });
    
                });
    
            }
            else{
    
                $scope.event = {
                    "visibility": ""
                };
    
    
                $scope.myAnswer = false;
                var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'partials/meeting.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen,
                    onRemoving: setAnswer,
                    locals: {
                        event: $scope.event,
                        hideCreatingMeeting: hideCreatingMeeting
                    }
                });
            }
        };

        $scope.logoutFromApp = function () {
            var logoutResource = $resource($rootScope.SERVER_BASE_URL+"/oauth/revoke-token");
            logoutResource.save(null,{accessToken:JSON.parse(localStorage.getItem('accessToken'))});
            $location.path('/login');
        };

        function setAnswer(){

            $scope.isEdited = false;

            if($rootScope.actualFilter == 1){
                $scope.getUnconfirmed();
            }
            else if($rootScope.actualFilter == 2){
                $scope.getOwn();
            }
            else if($rootScope.actualFilter == 3){
                $scope.getDeclined();
            }
            else if($rootScope.actualFilter == 4){
                $scope.getOld();
            }
            else{
                $scope.getHome();
            }

            $rootScope.user.isGoing = undefined;
            $rootScope.user.isNotGoing = undefined;
            $rootScope.user.isInvited = undefined;
            $rootScope.user.isAuthor = undefined;

            $scope.myAnswer = true;
        }
        
        $scope.keyword = "";
        var typingTimer;
        $scope.getSearchedEvents = function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function(){searchEvents()},400);
            return true;
        };
        function searchEvents() {
            if($scope.keyword.length > 2) {
                var eventSearchResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/search?size=" + $rootScope.MAX_INT);
                eventSearchResource.save(null, {keyword: $scope.keyword}, function (events) {
                    $rootScope.events = events.content;
                });
            }
            else if($scope.keyword.length == 0){
                var homeResource = $resource($rootScope.SERVER_BASE_URL+"/api/events/confirmed?sort=timestamp,asc&size="+$rootScope.MAX_INT);
                var home = homeResource.get(null, function () {
                    $rootScope.events = home.content;
                });
                $scope.eventsLabel = 'EVENTS';
                $rootScope.actualFilter = 0;
            }
        }

}]);



stuMeeAppControllers.controller('registerController',['$scope','$rootScope','$mdSidenav','$http','$mdDialog','$location', function ($scope, $rootScope, $mdSidenav, $http, $mdDialog,$location ) {

    $rootScope.backgroundValue = "rgba(0, 0, 0, 0.25)";

    $scope.AISpassShow = false;

    $rootScope.burgerSidenavShow = false;

    $scope.verifiedClass = 'secondary-color';


    $scope.person = {
        username: '',
        stuEmail: '',
        stuPassword: ''
    };

    $scope.verifyData = {
        suggestKey : '',
        maxItems : 3
    };
    
    $scope.unverified = true;

    $scope.verifyEmail = function () {

        if($scope.person.stuEmail != null &&  $scope.person.stuEmail != '' ){
            $scope.verifyData.suggestKey = $scope.person.stuEmail;
            $http({
                url: $rootScope.SERVER_BASE_URL+"/api/ais/users/search",
                method: 'POST',
                headers : {'Authorization':undefined},
                data: $scope.verifyData
            }).success(function(data){

                
                if(data.content.length !=0 ){
                    if(data.content.length == 1){
                        var user = data.content[0];
                        var surname = user.wholeName.split(' ')[0];
                        var name = user.wholeName.split(' ')[1];
                        name = name.substring(0,name.length-1);
                        $scope.person.name = name;
                        $scope.person.surname = surname;
                        $scope.person.id = user.id;
                        $scope.unverified = false;
                        $scope.verifiedClass = 'md-public-background';

                    }
                    else{
                        $mdDialog.show({
                            clickOutsideToClose: true,
                            templateUrl: 'partials/customUsersVerify.html',
                            parent: angular.element(document.querySelector('#registerForm')),
                            locals: {
                                data: data
                            },
                            onRemoving: setVerifyPerson,
                            controller: function DialogControl($scope, $mdDialog,data, $rootScope) {
                                $scope.data = data;
                                $scope.closeDialog = function() {
                                    $mdDialog.hide();
                                };
                                $scope.choosePerson = function (person) {
                                    var user = person;
                                    var surname = user.wholeName.split(' ')[0];
                                    var name = user.wholeName.split(' ')[1];
                                    name = name.substring(0,name.length-1);
                                    $rootScope.verifyPerson.name = name;
                                    $rootScope.verifyPerson.surname = surname;
                                    $rootScope.verifyPerson.id = user.id;
                                    $mdDialog.hide();
                                }
                            }
                        });

                    }

                }
                else{
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#registerForm')))
                            .clickOutsideToClose(true)
                            .title('Unverified user')
                            .textContent('This email is not in database.')
                            .ariaLabel('Alert')
                            .ok('OK')
                    );
                }

            }).error(function(err){ console.log(err)})
        }

    };

    function setVerifyPerson() {
        $scope.person.name = $rootScope.verifyPerson.name;
        $scope.person.surname = $rootScope.verifyPerson.surname;
        $scope.person.id = $rootScope.verifyPerson.id;
        $scope.unverified = false;
        $scope.verifiedClass = 'md-public-background';
    }

    $scope.submitForm = function() {
        $http({
            url: $rootScope.SERVER_BASE_URL+"/api/users",
            method: 'POST',
            headers : {'Authorization':undefined},
            data:  $scope.person
        }).success(function(data){
            $location.url('/login');
        }).error(function(err){
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#registerForm')))
                    .clickOutsideToClose(true)
                    .title('Registration problem')
                    .textContent('Your email or password is wrong.')
                    .ariaLabel('Alert')
                    .ok('OK')
            );
        })
    };

}]);

stuMeeAppControllers.controller('mainController',['$scope','$rootScope','$mdSidenav', function ($scope, $rootScope, $mdSidenav ) {

    $scope.toggleList = function(){
        $mdSidenav('left').toggle();
    };

    $rootScope.burgerSidenavShow = true;

}]);


stuMeeAppControllers.controller('profileController',['$scope','$rootScope','$mdMedia','$resource','fileUpload',  function ($scope, $rootScope, $mdMedia, $resource, fileUpload) {

    $scope.saveProfile = function () {

        var data = {
            username: $rootScope.user.username,
            surname: $rootScope.user.surname,
            name: $rootScope.user.name,
            stuEmail: $rootScope.user.stuEmail,
            secondaryEmail: $rootScope.user.secondaryEmail,
            description: $rootScope.user.description
        };

        var userUpdateResource = $resource($rootScope.SERVER_BASE_URL+"/api/users/:userId", null,
        {
            'update': { method:'PATCH' }
        });

        userUpdateResource.update({ userId: $rootScope.user.id },data);
    };

    $rootScope.editPhoto = false;

    $scope.uploadPhoto = function () {
        var file = $scope.myFile;
        var uploadUrl = $rootScope.SERVER_BASE_URL+"/api/users/"+$rootScope.user.id+'/picture';
        fileUpload.uploadFileToUrl(file, uploadUrl);
        $rootScope.editPhoto = false;
        $rootScope.user.canDelete = true;
    };

    $scope.deletePhoto = function () {

        var userFotoResource = $resource($rootScope.user.links.picture);
        var userFoto = userFotoResource.delete(null, function () {
            $rootScope.user.imageLink = "../assets/images/avatar.jpg";
            $rootScope.user.canDelete = false;
        });
    };

   /* $scope.showEditPhoto = function () {
        $scope.editPhoto = true;
    };*/


}]);


stuMeeAppControllers.controller('loginController',['$scope','$rootScope','$http','$httpParamSerializerJQLike','$mdDialog', function ($scope,$rootScope,$http,$httpParamSerializerJQLike,$mdDialog) {

    $scope.submitForm = function() {

        $http({
            url: $rootScope.SERVER_BASE_URL+"/login",
            method: 'POST',
            headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},
            data: $httpParamSerializerJQLike({stuEmail: $scope.login.stuEmail, stuPassword: $scope.login.stuPassword })

        }).success(function(data){

            window.location.replace($rootScope.SERVER_BASE_URL+"/oauth/authorize?client_id=stumee-web-client&response_type=token&redirect_uri="+$rootScope.SERVER_BASE_URL);

        }).error(function(err){
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#registerForm')))
                    .clickOutsideToClose(true)
                    .title('Authentication Failed')
                    .textContent('Wrong email or password.')
                    .ariaLabel('Alert')
                    .ok('OK')
            );
        })
    };
}]);
