/*!
 * Angular Material Design
 * https://github.com/angular/material
 * @license MIT
 * v1.0.0-rc7-master-a4ea9de
 */
goog.provide('ng.material.components.whiteframe');

/**
 * @ngdoc module
 * @name material.components.whiteframe
 */
angular.module('material.components.whiteframe', []);

ng.material.components.whiteframe = angular.module("material.components.whiteframe");