/*************************************/
/************* OAUTH2 ****************/

DROP TABLE IF EXISTS oauth_client_details;
DROP TABLE IF EXISTS oauth_client_token;
DROP TABLE IF EXISTS oauth_access_token;
DROP TABLE IF EXISTS oauth_refresh_token;
DROP TABLE IF EXISTS oauth_code;
DROP TABLE IF EXISTS oauth_approvals;

CREATE TABLE oauth_client_details (
  client_id               VARCHAR(255) PRIMARY KEY,
  resource_ids            VARCHAR(255),
  client_secret           VARCHAR(255),
  scope                   VARCHAR(255),
  authorized_grant_types  VARCHAR(255),
  web_server_redirect_uri VARCHAR(255),
  authorities             VARCHAR(255),
  access_token_validity   INTEGER,
  refresh_token_validity  INTEGER,
  additional_information  VARCHAR(255),
  autoapprove             VARCHAR(255)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE oauth_client_token (
  token_id          VARCHAR(255),
  token             BLOB,
  authentication_id VARCHAR(255) PRIMARY KEY,
  user_name         VARCHAR(255),
  client_id         VARCHAR(255)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE oauth_access_token (
  token_id          VARCHAR(255),
  token             BLOB,
  authentication_id VARCHAR(255) PRIMARY KEY,
  user_name         VARCHAR(255),
  client_id         VARCHAR(255),
  authentication    BLOB,
  refresh_token     VARCHAR(255)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE oauth_refresh_token (
  token_id       VARCHAR(255),
  token          BLOB,
  authentication BLOB
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE oauth_code (
  code           VARCHAR(255),
  authentication BLOB
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE oauth_approvals (
  userId         VARCHAR(255),
  clientId       VARCHAR(255),
  scope          VARCHAR(255),
  status         VARCHAR(10),
  expiresAt      TIMESTAMP,
  lastModifiedAt TIMESTAMP
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

/*************************************/
/******** StuMee Domain Model ********/

DROP TABLE IF EXISTS `events_attachments`;
DROP TABLE IF EXISTS `events_authors`;
DROP TABLE IF EXISTS `events_comments`;
DROP TABLE IF EXISTS `events_going`;
DROP TABLE IF EXISTS `events_invited`;
DROP TABLE IF EXISTS `events_not_going`;
DROP TABLE IF EXISTS `users_pictures`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `events`;

CREATE TABLE `users` (
  `ais_id`                    BIGINT(20)   NOT NULL,
  `ais_auth_token`            VARCHAR(255) DEFAULT NULL,
  `ais_auth_token_expiration` DATETIME     DEFAULT NULL,
  `confirmed`                 TINYINT(1)   NOT NULL,
  `deactivated`               TINYINT(1)   NOT NULL,
  `description`               VARCHAR(255) DEFAULT NULL,
  `name`                      VARCHAR(255) NOT NULL,
  `secondary_email`           VARCHAR(255) DEFAULT NULL,
  `stu_email`                 VARCHAR(255) NOT NULL,
  `surname`                   VARCHAR(255) NOT NULL,
  `username`                  VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ais_id`),
  UNIQUE KEY `UK_qyuxgfgk0uc9p7qt8fb9pspv3` (`stu_email`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
CREATE TABLE `users_pictures` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `type`      VARCHAR(255) NOT NULL,
  `created`   DATETIME     NOT NULL,
  `extension` VARCHAR(255) NOT NULL,
  `name`      VARCHAR(255) NOT NULL,
  `size`      BIGINT(20)   NOT NULL,
  `user_id`      BIGINT(20)   NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hx6sob2hhou04d0lr2yo5wqn8` (`user_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 100;

CREATE TABLE `events` (
  `id`          BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `canceled`    TINYINT(1)   NOT NULL,
  `created`     DATETIME     NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `location`    VARCHAR(255) NOT NULL,
  `timestamp`   DATETIME     NOT NULL,
  `title`       VARCHAR(255) NOT NULL,
  `type`        VARCHAR(255) NOT NULL,
  `visibility`  VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 100;

CREATE TABLE `events_attachments` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `caption`   VARCHAR(255)          DEFAULT NULL,
  `type`      VARCHAR(255) NOT NULL,
  `created`   DATETIME     NOT NULL,
  `extension` VARCHAR(255) NOT NULL,
  `name`      VARCHAR(255) NOT NULL,
  `size`      BIGINT(20)   NOT NULL,
  `event_id`     BIGINT(20)   NOT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_a9cfgmm0sbi8ooo9fikl6tdxl` (`event_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 100;

CREATE TABLE `events_authors` (
  `authored_events_id` BIGINT(20) NOT NULL,
  `authors_ais_id`     BIGINT(20) NOT NULL,
  PRIMARY KEY (`authored_events_id`, `authors_ais_id`),
  KEY `FK_1xuw2r8f2m8ydxjo32y33y3v4` (`authors_ais_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `events_comments` (
  `id`             BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `content`        VARCHAR(255) NOT NULL,
  `timestamp`      DATETIME     NOT NULL,
  `author_id`      BIGINT(20)   NOT NULL,
  `event_id`       BIGINT(20)   NOT NULL,
  `parent_comment` BIGINT(20)            DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lhtaftd0f3xvj1rdh3b4x2idc` (`author_id`),
  KEY `FK_hnm1in4lm9xw4e83yt73ajiqr` (`event_id`),
  KEY `FK_ee53q3mxvearfb22qjcw6gqkr` (`parent_comment`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 100;

CREATE TABLE `events_going` (
  `going_to_events_id` BIGINT(20) NOT NULL,
  `going_ais_id`       BIGINT(20) NOT NULL,
  PRIMARY KEY (`going_to_events_id`, `going_ais_id`),
  KEY `FK_iubeino87wuoy15apetm626mg` (`going_ais_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `events_invited` (
  `invited_to_events_id` BIGINT(20) NOT NULL,
  `invited_ais_id`       BIGINT(20) NOT NULL,
  PRIMARY KEY (`invited_to_events_id`, `invited_ais_id`),
  KEY `FK_p0x29xp2nxflqv02694x2hgac` (`invited_ais_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `events_not_going` (
  `not_going_to_events_id` BIGINT(20) NOT NULL,
  `not_going_ais_id`       BIGINT(20) NOT NULL,
  PRIMARY KEY (`not_going_to_events_id`, `not_going_ais_id`),
  KEY `FK_63bs7xipsrlu8nf6wmmslf8fh` (`not_going_ais_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

ALTER TABLE `events_attachments`
  ADD CONSTRAINT `FK_a9cfgmm0sbi8ooo9fikl6tdxl` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);

ALTER TABLE `events_authors`
  ADD CONSTRAINT `FK_3936mqld0uby10arl55fstja9` FOREIGN KEY (`authored_events_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `FK_1xuw2r8f2m8ydxjo32y33y3v4` FOREIGN KEY (`authors_ais_id`) REFERENCES `users` (`ais_id`);

ALTER TABLE `events_comments`
  ADD CONSTRAINT `FK_ee53q3mxvearfb22qjcw6gqkr` FOREIGN KEY (`parent_comment`) REFERENCES `events_comments` (`id`),
  ADD CONSTRAINT `FK_hnm1in4lm9xw4e83yt73ajiqr` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `FK_lhtaftd0f3xvj1rdh3b4x2idc` FOREIGN KEY (`author_id`) REFERENCES `users` (`ais_id`);

ALTER TABLE `events_going`
  ADD CONSTRAINT `FK_3wu57pfyvgnqpcubunxwypsjy` FOREIGN KEY (`going_to_events_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `FK_iubeino87wuoy15apetm626mg` FOREIGN KEY (`going_ais_id`) REFERENCES `users` (`ais_id`);

ALTER TABLE `events_invited`
  ADD CONSTRAINT `FK_pymmdwruuwh7a280w6982q3bc` FOREIGN KEY (`invited_to_events_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `FK_p0x29xp2nxflqv02694x2hgac` FOREIGN KEY (`invited_ais_id`) REFERENCES `users` (`ais_id`);

ALTER TABLE `events_not_going`
  ADD CONSTRAINT `FK_exfs0kbr3cwro0ektgenweatm` FOREIGN KEY (`not_going_to_events_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `FK_63bs7xipsrlu8nf6wmmslf8fh` FOREIGN KEY (`not_going_ais_id`) REFERENCES `users` (`ais_id`);

ALTER TABLE `users_pictures`
  ADD CONSTRAINT `FK_hx6sob2hhou04d0lr2yo5wqn8` FOREIGN KEY (`user_id`) REFERENCES `users` (`ais_id`);