INSERT INTO `events` (`id`, `canceled`, `created`, `description`, `location`, `timestamp`, `title`, `type`, `visibility`) VALUES
  (100, 0, '2016-05-11 00:00:00', 'home event 1', 'home event 1', '2017-06-11 00:00:00', 'home event 1', 'OTHER', 'PUBLIC'),
  (101, 0, '2016-07-11 00:00:00', 'home event 2', 'home event 2', '2017-05-19 00:00:00', 'home event 2', 'OTHER', 'PRIVATE'),
  (102, 0, '2016-05-11 00:00:00', 'home event 3', 'home event 3', '2017-05-24 00:00:00', 'home event 3', 'OTHER', 'PUBLIC'),
  (103, 0, '2016-05-11 00:00:00', 'home event not accessible', 'home event not accessible', '2017-05-20 00:00:00', 'home event not accessible', 'OTHER', 'PRIVATE'),
  (104, 0, '2016-05-11 00:00:00', 'unconfirmed event 1', 'unconfirmed event 1', '2017-05-20 00:00:00', 'unconfirmed event 1', 'OTHER', 'PRIVATE'),
  (105, 0, '2016-05-11 00:00:00', 'unconfirmed event 2', 'unconfirmed event 2', '2017-05-31 00:00:00', 'unconfirmed event 2', 'OTHER', 'PRIVATE'),
  (106, 0, '2016-05-11 00:00:00', 'unconfirmed event 3', 'unconfirmed event 3', '2017-05-21 00:00:00', 'unconfirmed event 3', 'OTHER', 'PUBLIC'),
  (107, 0, '2016-05-11 00:00:00', 'unconfirmed event not accessible', 'unconfirmed event not accessible', '2017-06-16 00:00:00', 'unconfirmed event not accessible', 'OTHER', 'PRIVATE'),
  (108, 1, '2016-05-09 00:00:00', 'authored event 1', 'authored event 1', '2016-05-09 00:00:00', 'authored event 1', 'OTHER', 'PRIVATE'),
  (109, 0, '2016-05-03 00:00:00', 'authored event 2', 'authored event 2', '2017-06-09 00:00:00', 'authored event 2', 'OTHER', 'PUBLIC'),
  (110, 0, '2016-05-10 00:00:00', 'declinded event 1', 'declinded event 1', '2017-05-19 00:00:00', 'declinded event 1', 'OTHER', 'PRIVATE'),
  (111, 0, '2016-05-10 00:00:00', 'declinded event 2', 'declinded event 2', '2017-05-26 00:00:00', 'declinded event 2', 'OTHER', 'PUBLIC'),
  (112, 0, '2016-05-10 00:00:00', 'old event 1', 'old event 1', '2016-05-10 00:00:00', 'old event 1', 'OTHER', 'PRIVATE'),
  (113, 1, '2016-05-10 00:00:00', 'old event 2', 'old event 2', '2016-05-09 00:00:00', 'old event 2', 'OTHER', 'PRIVATE'),
  (114, 0, '2016-05-10 00:00:00', 'old event 3', 'old event 3', '2016-05-08 00:00:00', 'old event 3', 'OTHER', 'PRIVATE'),
  (115, 0, '2016-05-10 00:00:00', 'old event 4', 'old event 4', '2016-05-07 00:00:00', 'old event 4', 'OTHER', 'PUBLIC'),
  (116, 1, '2016-05-10 00:00:00', 'old event 5', 'old event 5', '2016-05-01 00:00:00', 'old event 5', 'OTHER', 'PUBLIC'),
  (117, 1, '2016-05-08 00:00:00', 'old event not accessible', 'old event not accessible', '2016-05-09 00:00:00', 'old event not accessible', 'OTHER', 'PRIVATE');

INSERT INTO `users` (`ais_id`, `ais_auth_token`, `ais_auth_token_expiration`, `confirmed`, `deactivated`, `description`, `name`, `secondary_email`, `stu_email`, `surname`, `username`) VALUES
  (53897, NULL, NULL, 1, 0, 'Vývojár 1', 'Andrej', NULL, 'xkosara1@stuba.sk', 'Kosár', 'andrejkosar');

--
-- Dumping data for table `events_authors`
--

INSERT INTO `events_authors` (`authored_events_id`, `authors_ais_id`) VALUES
  (108, 53897),
  (109, 53897);

--
-- Dumping data for table `events_going`
--

INSERT INTO `events_going` (`going_to_events_id`, `going_ais_id`) VALUES
  (100, 53897),
  (101, 53897),
  (102, 53897),
  (113, 53897);

--
-- Dumping data for table `events_invited`
--

INSERT INTO `events_invited` (`invited_to_events_id`, `invited_ais_id`) VALUES
  (104, 53897),
  (105, 53897),
  (112, 53897),
  (115, 53897);

--
-- Dumping data for table `events_not_going`
--

INSERT INTO `events_not_going` (`not_going_to_events_id`, `not_going_ais_id`) VALUES
  (110, 53897),
  (111, 53897),
  (114, 53897);

INSERT INTO `oauth_client_details`
(`client_id`, `resource_ids`, `client_secret`,
 `scope`,
 `authorized_grant_types`, `web_server_redirect_uri`, `authorities`,
 `access_token_validity`, `refresh_token_validity`, `additional_information`,
 `autoapprove`)
VALUES
  ('stumee-native-client', 'stumee-api-resource', 'u2DJ27kwvRgl6qkb',
                           'get_public_events,get_private_events,create_event,get_event,update_event,cancel_event,get_profiles,create_profile,get_profile,update_profile,deactivate_profile',
                           'refresh_token,password', '', 'ROLE_USER',
                           3600, 86400, '{}',
                           'get_profiles,update_profile,deactivate_profile,get_event,create_event,get_private_events,create_profile,update_event,get_public_events,get_profile,cancel_event'),
  ('stumee-web-client', 'stumee-api-resource', NULL,
                        'get_public_events,get_private_events,create_event,get_event,update_event,cancel_event,get_profiles,create_profile,get_profile,update_profile,deactivate_profile',
                        --                        'implicit', 'http://localhost:8080', 'ROLE_USER',
                        'implicit', 'http://stumee-andrejkosar.rhcloud.com', 'ROLE_USER',
                        6000, NULL, '{}',
                        'get_profiles,update_profile,deactivate_profile,get_event,create_event,get_private_events,create_profile,update_event,get_public_events,get_profile,cancel_event');
